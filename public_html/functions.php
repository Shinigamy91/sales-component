<?php

//function filter_by($item, $hotel){
//	//return($var & 1);
//	return($item->hotel->name == 'Fifty Hotel and Suites by Affinia');
//}

function filter_by_hotel($holidays, $hotel){
	
	foreach($holidays as $holiday){
		
		if($holiday->hotel->id == $hotel){
			//var_dump($holiday);
			return $holiday;
		}
	}
	
};

function filter_by_route($routes, $origin, $destination){
	
	foreach($routes as $route){
		
		//echo "ORIGIN " . $origin; 
		//echo "DEST " . $destination; 
		//echo "ROUTE HERE";
		//var_dump($route);
		if($route->origin_queried == $origin && $route->destination_queried == $destination){
			//var_dump($holiday);
			return $route;
		}
	}
	
};

function getRegionFromDestination($destination) {
	switch ($destination) {
		case 'ANU':
		case 'BGI':
		case 'GND':
		case 'MBJ':
		case 'UVF':
		case 'TAB':
			return 'caribbean';
			break;
		case 'HKG':
		case 'SHA':
		case 'BOM':
			return 'asia';
			break;
		case 'LAS':
		case 'LAX':
		case 'SFO':
		case 'SEA':
			return 'west-coast';
			break;
		case 'TLV':
		case 'JNB':
		case 'CPT':
		case 'DUR':
			return 'africa-israel';
			break;
		case 'JFK':
		case 'BOS':
		case 'WAS':
		case 'ATL':
		case 'MCO':
		case 'MIA':
			return 'east-coast';
			break;
	}
}

function formatRegionObject($regionCode) {
	$region = new stdClass();

	switch ($regionCode) {
		case 'caribbean':
			$region->code = 'caribbean';
			$region->formatted = 'Caribbean';
			break;
		case 'asia':
			$region->code = 'asia';
			$region->formatted = 'Asia';
			break;
		case 'west-coast':
			$region->code = 'west-coast';
			$region->formatted = 'West Coast';
			break;
		case 'africa-israel':
			$region->code = 'africa-israel';
			$region->formatted = 'Africa & Israel';
			break;
		case 'east-coast':
			$region->code = 'east-coast';
			$region->formatted = 'East Coast';
			break;
		default:
			$region->code = 'unkown';
			$region->formatted = 'Unknown';
			break;
	}

	return $region;
}


function getFlightHotelPrice($origin, $destination, $departureDate, $hotel){
	
	//$origin = 'LGW';
	//$destination = 'orlando';
	$bookingType = 'holiday';
	//day-month-year
	$departureDate = '11-09-2019';//this is being hardcoded inatm however you have the option to use the dynamic argument var instead.
	$duration = '8';

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://travelplus.virginatlantic.com/travelplus/cjs-search-api/search",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\"location\":\"".$destination."\",\"gateway\":\"".$origin."\",\"bookingType\":\"".$bookingType."\",\"departureDate\":\"".$departureDate."\",\"duration\":\"".$duration."\",\"partyCompositions\":[{\"adults\":1,\"childAges\":[],\"infants\":0}]}",
	  CURLOPT_HTTPHEADER => array(
		//"Postman-Token: 707cd561-ae1d-4fa1-b4d9-01eca72c3c85",
		"cache-control: no-cache",
		"content-type: application/json"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	  //$flighthotel = $response;
	  //var_dump($response);
		$ob = json_decode($response);
		$holidays = $ob->holidays;
		//echo $hotel;
		
		//hotel is an ID value
		$holiday = filter_by_hotel($holidays, $hotel);
		//var_dump($holiday);
		
		
//		foreach($holidays as $holiday){
//			echo "HOLIDAY START";
//			var_dump($holiday);
//			echo "HOLIDAY END";
//		}
		
		//holiday ob has loads of vals available, just returning price atm
		//echo "<pre>";
		//var_dump($holiday);
		//echo "</pre>";
		//echo "==============";
		//echo gettype($holiday), "\n";
		$hol_type = gettype($holiday);
		if($hol_type == 'NULL'){
			echo "=====================";
			echo "ISSUE GETTING HOLIDAY WITH HOTEL ID " . $hotel;
			echo "<pre>";
			var_dump($holidays);
			echo "</pre>";
			echo "=====================";
		}
		$price = $holiday->totalPrice;
		return $price;
		
		
	}
	
}

function getFlightPrice($origin, $destination){
	
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://virgin-api.xcheck.co/v1/c0000y2733",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_POSTFIELDS => "",
	  CURLOPT_HTTPHEADER => array(
		//"Postman-Token: f3067716-479d-4f47-81f1-c8ca77798ac0",
		"cache-control: no-cache"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  //echo $response;
	  //$flighthotel = $response;
	  //var_dump($response);
		$response = json_decode($response);
		$routes = $response->routes;
		//var_dump($routes);
		//$holidays = $ob->holidays;
		//echo $hotel;
		
		//hotel is an ID value
		$route = filter_by_route($routes, $origin, $destination);
		//var_dump($holiday);
		
		
//		foreach($holidays as $holiday){
//			echo "HOLIDAY START";
//			var_dump($holiday);
//			echo "HOLIDAY END";
//		}
		
		//holiday ob has loads of vals available, just returning price atm
		//var_dump($route);
		$price = $route->price;
		return $price;
		
		
	}
	
}


function generateLink($rw, $id = ''){
	//container_link
	$link = '';
	
	//not using this for summer sale at present
	if($rw->container_link == 'TripSummary'){
		
		//trip summary link
		$link = 'https://www.virginatlantic.com/air-shopping/priceTripAction.action?dispatchMethod=priceItin&paxCounts[0]=2&paxCounts[1]=0&paxCounts[2]=0&paxCounts[3]=0&tripType=roundtrip&cabin=';
		$link .= $rw->cabin_formatted;
		$link .= '&itinSegment[0]=0:M:';
		$link .= $rw->origin_code;
		$link .= ':';
		$link .= $rw->destination_code_formatted;
		$link .= ':';
		$link .= $rw->outbound_flight_formatted;
		$link .= ':';
		$link .= $rw->outbound_date_formatted;
		$link .= ':10A&itinSegment[1]=1:S:';
		$link .= $rw->destination_code_formatted;
		$link .= ':';
		$link .= $rw->origin_code;
		$link .= ':';
		$link .= $rw->inbound_flight_formatted;
		$link .= ':';
		$link .= $rw->inbound_date_formatted;
		$link .= ':20P&numOfSegments=2&icid=wintersale2019';
		
		
		
	//} elseif($rw->container_link == 'FlightSearch'){
	} elseif($rw->container_link == 'FlexiCal'){
		
		//https://www.virginatlantic.com/air-shopping/findFlights.action?action=findFlights&
//		tripType=ROUND_TRIP&
//			priceSchedule=PRICE&
//			originCity=LGW&
//			destinationCity=BGI&
//			departureDate=03/12/2019&
//			departureTime=AT&
//			returnDate=03/19/2019&
//			returnTime=AT&
//			paxCount=1&
//			searchByCabin=true&
//			cabinFareClass=VSPE&
//			deltaOnlySearch=false&
//			deltaOnly=off&
//			Go=Find%20Flights&meetingEventCode=&
//			refundableFlightsOnly=false&
//			compareAirport=false&
//			awardTravel=false&
//			datesFlexible=undefined&
//			flexAirport=false&
//			paxCounts[0]=2&
//			paxCounts[1]=0&
//			paxCounts[2]=0&
//			paxCounts[3]=0
		
		//pre IDP
		//$link = 'https://www.virginatlantic.com/air-shopping/findFlights.action?';
		//IDP link
		$link = 'https://www.virginatlantic.com/flight-search-2/search?';
		$link .= 'action=findFlights&';
		$link .= 'tripType=ROUND_TRIP&';
		//$link .= 'priceSchedule=PRICE&';
		$link .= 'priceSchedule=price&';
		$link .= 'originCity='. $rw->origin_code .'&';
		//$link .= 'destinationCity='. $rw->destination_code_formatted .'&';
		$link .= 'destinationCity='. $rw->destination_code_formatted .'&';
		$link .= 'departureDate='.  $rw->outbound_date_formatted  .'&';
		$link .= 'departureTime=AT&';
		$link .= 'returnDate=' . $rw->inbound_date_formatted . '&';
		$link .= 'returnTime=AT&';
		//$link .= 'paxCount=1&';
		$link .= 'searchByCabin=true&';
		$link .= 'cabinFareClass='. $rw->cabin_formatted .'&';
		$link .= 'deltaOnlySearch=false&';
		$link .= 'deltaOnly=off&';
		$link .= 'Go=Find%20Flights&meetingEventCode=&';
		$link .= 'refundableFlightsOnly=false&';
		$link .= 'compareAirport=false&';
		$link .= 'awardTravel=false&';
		$link .= 'datesFlexible=true&';//change to undefined to not go to flexicalendar
		//$link .= 'flexAirport=false&';
		$link .= 'flexAirport=false&';
//		$link .= 'paxCounts[0]=2&';
//		$link .= 'paxCounts[1]=0&';
//		$link .= 'paxCounts[2]=0&';
//		$link .= 'paxCounts[3]=0';
		$link .= 'passengerInfo=ADT:2|';
		$link .= 'GBE:0|';
		$link .= 'CNN:0|';
		$link .= 'INF:0';
		//$link .= '&cm_mmc=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . strtoupper($rw->destination_code) . '_' . removeSpaces($rw->outbound_date_visual) . '_' . removeSpaces($rw->inbound_date_visual) . '_' . removeSpaces($rw->cabin_visual);
		$link .= '&intcmp=LM19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . strtoupper($rw->destination_code) . '_' . removeSpaces($rw->outbound_date_visual) . '_' . removeSpaces($rw->inbound_date_visual) . '_' . removeSpaces($rw->cabin_visual);
		
		//SS19_ID1_FLIGHT _LHR_JFK_2NOV19_4NOV19_CLASSIC
		
	} elseif($rw->container_link == 'TravelPlus'){
	
		
		//https://travelplus.virginatlantic.com/flight+hotel/boston?departureDate=20-03-2019&duration=3&gateway=LHR&room=a2,i0&cm_mmc=Virgin%20Atlantic-_-Flight%20Search-_-Flight%20And%20Hotel-_-&utm_campaign=Midtown%20Boston%20One&utm_source=virginatlantic.com&utm_medium=referral 
		
		//$link = 'https://travelplus.virginatlantic.com/flight+hotel/'. $rw->destination_code_formatted .'?';
		$link = 'https://travelplus.virginatlantic.com/flight+hotel/'. $rw->travelplusDestName .'?';
		$link .= 'departureDate='. $rw->outbound_date_formatted .'&';
		$link .= 'duration='. $rw->duration .'&';
		$link .= 'gateway='.$rw->origin_code.'&';
		$link .= 'room=a2,i0';
		//SS19_ID4_FLIGHTHOTEL_LHR_MCO_UNIVERSAL CABANA BAY_8OCT19_4NOV19_PREMIUM
		//$link .= '&cm_mmc=Virgin%20Atlantic-_-Flight%20Search-_-Flight%20And%20Hotel-_-&utm_campaign=Midtown%20Boston%20One&utm_source=virginatlantic.com&utm_medium=referral';
		$link .= '&cm_mmc=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . addDashes($rw->hotel_name) . '_' . strtoupper($rw->destination_code) . '_' . removeSpaces($rw->outbound_date_visual) . '_' . removeSpaces($rw->inbound_date_visual) . '_' . removeSpaces($rw->cabin_visual);
		
		$link .= '&utm_source=virginatlantic.com&utm_medium=referral&utm_campaign=LM19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . addDashes($rw->hotel_name) . '_' . strtoupper($rw->destination_code) . '_' . removeSpaces($rw->outbound_date_visual) . '_' . removeSpaces($rw->inbound_date_visual) . '_' . removeSpaces($rw->cabin_visual);
	
	} elseif($rw->container_link == 'SearchResults'){
		
		//currently not being used but may send Economy Delight to this location
		//only difference between this and flexiCal is datesFlexible=true
		//Pre IDP url
		//$link = 'https://www.virginatlantic.com/air-shopping/findFlights.action?';
		//IDP url
		$link = 'https://www.virginatlantic.com/flight-search-2/search?';
		$link .= 'action=findFlights&';
		$link .= 'tripType=ROUND_TRIP&';
		$link .= 'priceSchedule=PRICE&';
		$link .= 'originCity='. $rw->origin_code .'&';
		//$link .= 'destinationCity='. $rw->destination_code_formatted .'&';
		$link .= 'destinationCity='. $rw->destination_code_formatted .'&';
		$link .= 'departureDate='.  $rw->outbound_date_formatted  .'&';
		$link .= 'departureTime=AT&';
		$link .= 'returnDate=' . $rw->inbound_date_formatted . '&';
		$link .= 'returnTime=AT&';
		$link .= 'paxCount=1&';
		$link .= 'searchByCabin=true&';
		$link .= 'cabinFareClass='. $rw->cabin_formatted .'&';
		$link .= 'deltaOnlySearch=false&';
		$link .= 'deltaOnly=off&';
		$link .= 'Go=Find%20Flights&meetingEventCode=&';
		$link .= 'refundableFlightsOnly=false&';
		$link .= 'compareAirport=false&';
		$link .= 'awardTravel=false&';
		$link .= 'datesFlexible=undefined&';//change to undefined to not go to flexicalendar
		$link .= 'flexAirport=false&';
		$link .= 'flexAirport=false&';
		$link .= 'paxCounts[0]=2&';
		$link .= 'paxCounts[1]=0&';
		$link .= 'paxCounts[2]=0&';
		$link .= 'paxCounts[3]=0';
		//$link .= '&cm_mmc=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . strtoupper($rw->destination_code) . '_' . removeSpaces($rw->outbound_date_visual) . '_' . removeSpaces($rw->inbound_date_visual) . '_' . removeSpaces($rw->cabin_visual);
	
	
	}
	
	
	return $link;
}

function genTrackingLink($rw, $id, $date){
	$str = '';
	//$outbound_d = outbound_date_visual($date);
	//$d = strtotime($str);
	//$d = formatdatefortracking($date, $rw->trip_type);
	
	$da = str_replace('/', '-', $date );
	$outbound = date("dMy", strtotime($da));
	$outbound = strtoupper($outbound);
	
	//echo "HERE" . $rw->container_link;
	
	if($rw->container_link == 'FlexiCal'){
		
		$returnDate = date("dMy", strtotime($da. ' + 7 days'));
		$returnDate = strtoupper($returnDate);
		
		
		$str = '&cm_mmc=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . strtoupper($rw->destination_code) . '_' . $outbound . '_' . $returnDate . '_' . removeSpaces($rw->cabin_visual);
		
	} elseif($rw->container_link == 'TravelPlus'){
		
		$duration = $rw->duration;
		
		$inbound = date("dMy", strtotime($da. ' + '.$duration.' days'));
		$inbound = strtoupper($inbound);
		
		$str = '&cm_mmc=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . addDashes($rw->hotel_name) . '_' . strtoupper($rw->destination_code) . '_' . $outbound . '_' . $inbound . '_' . removeSpaces($rw->cabin_visual) . '&utm_source=virginatlantic.com&utm_medium=referral&utm_campaign=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . addDashes($rw->hotel_name) . '_' . strtoupper($rw->destination_code) . '_' . $outbound . '_' . $inbound . '_' . removeSpaces($rw->cabin_visual);
		
		
	} elseif($rw->container_link == 'SearchResults'){
		
		$returnDate = date("dMy", strtotime($da. ' + 7 days'));
		$returnDate = strtoupper($returnDate);
		
		$str = '&cm_mmc=SS19_ID' . $id . '_' . removeSpaces($rw->trip_type) . '_' . strtoupper($rw->origin_code) . '_' . strtoupper($rw->destination_code) . '_' . $outbound . '_' . $returnDate . '_' . removeSpaces($rw->cabin_visual);
		
	}
	
	return $str;
}



function flightStatus($triptype){
	$str = '';
	if($triptype == 'Flight'){
		$str = 'flight';
	} else {
		$str = 'flighthotel';
	}
	
	return $str;
}

function flightClass($cabin){
	$str = '';
	if($cabin == 'Economy Classic') {
		$str = 'economy-classic';
	} else if ($cabin == 'Economy Light') {
		$str = 'economy-light';
	} else if ($cabin == 'Economy Delight') {
		$str = 'economy-delight';
	} elseif( $cabin == 'Premium') {
		$str = 'premium';
	} elseif( $cabin == 'Upper Class') {
		$str = 'upper';
	}
	
	return $str;
}

function tripType($type){
	$str = '';
		if($type == 'Flight'){
			$str = 'Flight Only/Round Trip';
		} else {
			$str = 'Flight &amp; Hotel';
		} 
	
	return $str;
}

function cabinVisual($cab){
	$str = '';

	if ($cab == 'Economy Light'){
		$str = "<span class=\"WS-loc-txt\"><span class=\"WS-loc-icn\"><img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Bag.svg\" style=\"width:16px;\"></span>Hand baggage only</span>";
	} else {
		$str = "<span class=\"WS-loc-txt\"><span class=\"WS-loc-icn\"><img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Bag.svg\" style=\"width:16px;\"></span>Baggage included</span>";
	} 

	if ($cab == 'Economy Classic') {
		$str = "<span class=\"WS-loc-txt\"><span class=\"WS-loc-icn\"><img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Bag.svg\" style=\"width:16px;\"></span>1 x 23kg baggage</span>";
	}

	if ($cab == 'Economy Delight') {
		$str = "<span class=\"WS-loc-txt\"><span class=\"WS-loc-icn\"><img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Bag.svg\" style=\"width:16px;\"></span>1 x 23kg baggage</span>";
	}

	if ($cab == 'Premium') {
		$str = "<span class=\"WS-loc-txt\"><span class=\"WS-loc-icn\"><img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Bag.svg\" style=\"width:16px;\"></span>2 x 23kg baggage</span>";
	}

	if ($cab == 'Upper Class') {
		$str = "<span class=\"WS-loc-txt\"><span class=\"WS-loc-icn\"><img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Bag.svg\" style=\"width:16px;\"></span>3 x 23kg baggage</span>";
	}
	
	return $str;
}

function airportCode($rw){
	$str = '';
	if($rw->trip_type == 'Flight'){
		$str = "<span class=\"flightCode\">(" . $rw->origin_code . ")</span><span class=\"dubbleArrow\"></span>";
	}
	
	return $str;
}

function destCode($val){
	$code = '';
	
	if( $val == 'New York'){
		$code = 'JFK';
	} elseif( $val == 'Boston' ){
		$code = 'BOS';
	}elseif( $val == 'Las Vegas' ){
		$code = 'LAS';
	}elseif( $val == 'Orlando' ){
		$code = 'MCO';
	}elseif( $val == 'Barbados' ){
		$code = 'BGI';
	}elseif( $val == 'Los Angeles' ){
		$code = 'LAX';
	}elseif( $val == 'Delhi' ){
		$code = 'DEL';
	}elseif( $val == 'San Francisco' ){
		$code = 'SFO';
	}elseif( $val == 'Johannesburg' ){
		$code = 'JNB';
	}elseif( $val == 'Miami' ){
		$code = 'MIA';
	}elseif( $val == 'Shanghai' ){
		$code = 'PVG';
	}elseif( $val == 'Washington' ){
		$code = 'IAD';
	}elseif( $val == 'Hong Kong' ){
		$code = 'HKG';
	}elseif( $val == 'Atlanta' ){
		$code = 'ATL';
	}elseif( $val == 'Seattle' ){
		$code = 'SEA';
	}elseif( $val == 'St Lucia' ){
		$code = 'UVF';
	}elseif( $val == 'Dubai' ){
		$code = 'DXB';
	}elseif( $val == 'Jamaica' ){
		$code = 'MBJ';
	}elseif( $val == 'Tobago' ){
		$code = 'TAB';
	}elseif( $val == 'Cuba' ){
		$code = 'HAV';
	}elseif( $val == 'Antigua' ){
		$code = 'ANU';
	}elseif( $val == 'Grenada' ){
		$code = 'GND';
	}elseif( $val == 'Tel Aviv' ){
		$code = 'TLV';
	}elseif( $val == 'Mumbai' ){
		$code = 'BOM';
	}elseif( $val == 'Sao Paulo' ){
		$code = 'GRU';
	} 
//	else {
//		$code = $val;
//	}
	
	return $code;
}
	
function stars($stars){
	$stars = (float) $stars;
	$html = "";
	for($i = 1; $i <= 5; $i++){
		$html .= "<span class=\"flag";
		if($i == 1){
			$html .= " one";
		} elseif($i == 2){
			$html .= " two";
		} elseif($i == 3){
			$html .= " three";
		} elseif($i == 4){
			$html .= " four";
		} elseif($i == 5){
			$html .= " five";
		}
		$html .= "\">";
		
		//echo "STARS: " . $stars;
		
		if($i <= $stars){
			if( $i + 0.5 == $stars){
				$html .= "<img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Triangle-half.svg\">";
			} else {
				$html .= "<img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Triangle-Red.svg\">";
			}
			
			
		} else {
			
		$html .= "<img src=\"https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Triangle.svg\">";
		}
		
		$html .= "</span>\n";
	}
	
	//$html .= "\n\n";
	
	return $html;
}

function origin_visual($code){
	
	$origin = '';
	
	if($code == 'LGW'){
		$origin = 'London Gatwick';
	} else if($code == 'LHR'){
		$origin = 'London Heathrow';
	} else if($code == 'MAN'){
		$origin = 'Manchester';
	} else if($code == 'BFS'){
		$origin = 'Belfast';
	} else if($code == 'GLA'){
		$origin = 'Glasgow';
	}
	
	return $origin;
}

function destination_visual($code){
	$destination = '';
	
	if($code == "ANU"){ $destination = 'Antigua'; }
	else if($code == "ATL"){ $destination = 'Atlanta'; }
	else if($code == "BOS"){ $destination = 'Boston'; }
	else if($code == "BGI"){ $destination = 'Barbados'; }
	else if($code == "HAV"){ $destination = 'Cuba'; }
	else if($code == "DEL"){ $destination = 'Delhi'; }
	else if($code == "DXB"){ $destination = 'Dubai'; }
	else if($code == "GND"){ $destination = 'Grenada'; }
	else if($code == "HKG"){ $destination = 'Hong Kong'; }
	else if($code == "JNB"){ $destination = 'Johannesburg'; }
	else if($code == "LOS"){ $destination = 'Lagos'; }
	else if($code == "LAS"){ $destination = 'Las Vegas'; }
	else if($code == "LAX"){ $destination = 'Los Angeles'; }
	else if($code == "MIA"){ $destination = 'Miami'; }
	else if($code == "MBJ"){ $destination = 'Montego Bay'; }
	else if($code == "JFK"){ $destination = 'New York'; }
	else if($code == "MCO"){ $destination = 'Orlando'; }
	else if($code == "SFO"){ $destination = 'San Francisco'; }
	else if($code == "SEA"){ $destination = 'Seattle'; }
	else if($code == "PVG"){ $destination = 'Shanghai'; }
	else if($code == "UVF"){ $destination = 'St Lucia'; }
	else if($code == "TAB"){ $destination = 'Tobago'; }
	else if($code == "IAD"){ $destination = 'Washington'; }
	else if($code == "TLV"){ $destination = 'Tel Aviv'; }
	else if($code == "BOM"){ $destination = 'Mumbai'; }
	else if($code == "GRU"){ $destination = 'Sao Paulo'; }
	
	return $destination;
}

//function data_destination($code){
//	$data_code = '';
//	
//	return $data_code;
//}

function destination_code_formatted($code){
	
	$val = '';
	
	//these codes need to be checked
	if($code == "ANU"){ $val = 'ANU'; }
	else if($code == "ATL"){ $val = 'ATL'; }
	else if($code == "BOS"){ $val = 'BOS'; }
	else if($code == "BGI"){ $val = 'BGI'; }
	else if($code == "HAV"){ $val = 'HAV'; }
	else if($code == "DEL"){ $val = 'DEL'; }
	else if($code == "DXB"){ $val = 'DXB'; } 
	else if($code == "GND"){ $val = 'GND'; }
	else if($code == "HKG"){ $val = 'HKG'; }
	else if($code == "JNB"){ $val = 'JNB'; }
	else if($code == "LOS"){ $val = 'LOS'; }
	else if($code == "LAS"){ $val = 'LAS'; }
	else if($code == "LAX"){ $val = 'LAX'; }
	else if($code == "MIA"){ $val = 'MIA'; }
	else if($code == "MBJ"){ $val = 'MBJ'; }
	else if($code == "JFK"){ $val = 'NYC'; }
	else if($code == "MCO"){ $val = 'MCO'; }
	else if($code == "SFO"){ $val = 'SFO'; }
	else if($code == "SEA"){ $val = 'SEA'; }
	else if($code == "PVG"){ $val = 'PVG'; }
	else if($code == "UVF"){ $val = 'UVF'; }
	else if($code == "TAB"){ $val = 'TAB'; }
	else if($code == "IAD"){ $val = 'IAD'; }
	else if($code == "TLV"){ $val = 'TLV'; }
	else if($code == "BOM"){ $val = 'BOM'; }
	else if($code == "GRU"){ $val = 'GRU'; }
	
	return $val;
	
}

function cabin_formatted($cabin){
	
	$cabin_code = '';
	
	//VA site doesnt seem to except VSCL and VSDT in cabinClass query param
	if($cabin == "Economy Light"){ $cabin_code = 'VSLT'; }
	else if($cabin == "Economy Classic"){ $cabin_code = 'VSCL'; }
	else if($cabin == "Economy Delight"){ $cabin_code = 'VSDT'; }
	else if($cabin == "Premium"){ $cabin_code = 'VSPE'; }
	else if($cabin == "Upper Class"){ $cabin_code = 'VSUP'; }
	
//	if($cabin == "Economy Light"){ $cabin_code = 'VSLT'; }
//	else if($cabin == "Economy Classic"){ $cabin_code = 'VSCL'; }
//	else if($cabin == "Economy Delight"){ $cabin_code = 'VSDT'; }
//	else if($cabin == "Premium"){ $cabin_code = 'VSPE'; }
//	else if($cabin == "Upper Class"){ $cabin_code = 'VSUP'; }
	
	//echo "CABIN" . $cabin_code;
	
	return $cabin_code;
	
}

function outbound_date_visual($str){
	$d = strtotime($str);
	
	//eg. 08 Jan 20
	$formatted_date = date("d M y", $d);
	
	return $formatted_date;
}


function flight_outbound_date_visual($str){
	$d = strtotime($str);
	//eg. 08 Jan
	$formatted_date = date("d M", $d);
	
	return $formatted_date;
}

function inbound_date_visual($str){
	$d = strtotime($str);
	//eg. 15 Jan 20
	$formatted_date = date("d M y", $d);
	
	return $formatted_date;
}

function outbound_date_formatted($type, $str){
	//var_dump($str);
	//$d = strtotime($str);
	//eg. Flight - 11/04/2019 , Flight+Hotel 17-11-2019
	$formatted_date = '';
	
	if($type == 'Flight'){
		$da = str_replace('/', '-', $str);
		//$formatted_date = date("d/m/Y", $d);
		$formatted_date = date("m/d/Y", strtotime($da));
		
	} else if($type == 'Flight + Hotel'){
		$d = strtotime($str);
		$formatted_date = date("d-m-Y", $d);
	}
	
	return $formatted_date;
}




function inbound_date_formatted($type, $str){
	//$d = strtotime($str);
	//eg. Flight - 11/04/2019 , Flight+Hotel 17-11-2019
	$formatted_date = '';
	
	if($type == 'Flight'){
		//$formatted_date = date("d/m/Y", $d);
		$da = str_replace('/', '-', $str);
		//$d = strtotime($da);
		$formatted_date = date("m/d/Y", strtotime($da));
	} else if($type == 'Flight + Hotel'){
		$d = strtotime($str);
		$formatted_date = date("d-m-Y", $d);
	}
	
	return $formatted_date;
}

function addSevenDays($date){
	
	$dateVal = "";
	$da = str_replace('/', '-', $date);
	$dateVal = date("m/d/Y", strtotime($da. ' + 7 days'));
	
	return $dateVal;
}


//function inbound_flight_formatted($origin, $dest){
function flight_formatted($origin, $dest){
	$str = '';
	if($origin== "LHR" && $dest == "EWR"){ $str = "1"; }
	else if($origin== "EWR" && $dest == "LHR"){ $str = "2"; }
	else if($origin== "LHR" && $dest == "JFK"){ $str = "3"; }
	else if($origin== "JFK" && $dest == "LHR"){ $str = "4"; }
	else if($origin== "LHR" && $dest == "MIA"){ $str = "5"; }
	else if($origin== "MIA" && $dest == "LHR"){ $str = "6"; }
	else if($origin== "LHR" && $dest == "LAX"){ $str = "7"; }
	else if($origin== "LAX" && $dest == "LHR"){ $str = "8"; }
	else if($origin== "LHR" && $dest == "JFK"){ $str = "9"; }
	else if($origin== "JFK" && $dest == "LHR"){ $str = "10"; }
	else if($origin== "LHR" && $dest == "BOS"){ $str = "11"; }
	else if($origin== "BOS" && $dest == "LHR"){ $str = "12"; }
	else if($origin== "LGW" && $dest == "MCO"){ $str = "15"; }
	else if($origin== "MCO" && $dest == "LGW"){ $str = "16"; }
	else if($origin== "LHR" && $dest == "EWR"){ $str = "17"; }
	else if($origin== "LHR" && $dest == "SFO"){ $str = "19"; }
	else if($origin== "SFO" && $dest == "LHR"){ $str = "20"; }
	else if($origin== "LHR" && $dest == "IAD"){ $str = "21"; }
	else if($origin== "IAD" && $dest == "LHR"){ $str = "22"; }
	else if($origin== "LHR" && $dest == "LAX"){ $str = "23"; }
	else if($origin== "LAX" && $dest == "LHR"){ $str = "24"; }
	else if($origin== "LHR" && $dest == "JFK"){ $str = "25"; }
	else if($origin== "JFK" && $dest == "LHR"){ $str = "26"; }
	else if($origin== "LGW" && $dest == "MCO"){ $str = "27"; }
	else if($origin== "MCO" && $dest == "LGW"){ $str = "28"; }
	else if($origin== "LGW" && $dest == "BGI"){ $str = "29"; }
	else if($origin== "BGI" && $dest == "LGW"){ $str = "30"; }
	else if($origin== "LGW" && $dest == "UVF"){ $str = "31"; }
	else if($origin== "UVF" && $dest == "LGW"){ $str = "32"; }
	else if($origin== "LGW" && $dest == "ANU"){ $str = "33"; }
	else if($origin== "ANU" && $dest == "LGW"){ $str = "34"; }
	else if($origin== "LHR" && $dest == "SFO"){ $str = "41"; }
	else if($origin== "SFO" && $dest == "LHR"){ $str = "42"; }
	else if($origin== "LHR" && $dest == "JFK"){ $str = "45"; }
	else if($origin== "JFK" && $dest == "LHR"){ $str = "46"; }
	else if($origin== "LGW" && $dest == "MCO"){ $str = "49"; }
	else if($origin== "MCO" && $dest == "LGW"){ $str = "50"; }
	else if($origin== "LGW" && $dest == "HAV"){ $str = "63"; }
	else if($origin== "HAV" && $dest == "LGW"){ $str = "64"; }
	else if($origin== "LGW" && $dest == "MBJ"){ $str = "65"; }
	else if($origin== "MBJ" && $dest == "LGW"){ $str = "66"; }
	else if($origin== "GLA" && $dest == "MCO"){ $str = "71"; }
	else if($origin== "MCO" && $dest == "GLA"){ $str = "72"; }
	else if($origin== "MAN" && $dest == "MCO"){ $str = "73"; }
	else if($origin== "MCO" && $dest == "MAN"){ $str = "74"; }
	else if($origin== "MAN" && $dest == "MCO"){ $str = "75"; }
	else if($origin== "MCO" && $dest == "MAN"){ $str = "76"; }
	else if($origin== "MAN" && $dest == "BGI"){ $str = "77"; }
	else if($origin== "BGI" && $dest == "MAN"){ $str = "78"; }
	else if($origin== "MAN" && $dest == "LAS"){ $str = "85"; }
	else if($origin== "LAS" && $dest == "MAN"){ $str = "86"; }
	else if($origin== "LGW" && $dest == "UVF"){ $str = "89"; }
	else if($origin== "UVF" && $dest == "GND"){ $str = "89"; }
	else if($origin== "GND" && $dest == "UVF"){ $str = "90"; }
	else if($origin== "UVF" && $dest == "LGW"){ $str = "90"; }
	else if($origin== "LGW" && $dest == "UVF"){ $str = "97"; }
	else if($origin== "UVF" && $dest == "TAB"){ $str = "97"; }
	else if($origin== "TAB" && $dest == "UVF"){ $str = "98"; }
	else if($origin== "UVF" && $dest == "LGW"){ $str = "98"; }
	else if($origin== "LHR" && $dest == "ATL"){ $str = "103"; }
	else if($origin== "ATL" && $dest == "LHR"){ $str = "104"; }
	else if($origin== "LHR" && $dest == "SEA"){ $str = "105"; }
	else if($origin== "SEA" && $dest == "LHR"){ $str = "106"; }
	else if($origin== "MAN" && $dest == "ATL"){ $str = "109"; }
	else if($origin== "ATL" && $dest == "MAN"){ $str = "110"; }
	else if($origin== "LHR" && $dest == "MIA"){ $str = "117"; }
	else if($origin== "MIA" && $dest == "LHR"){ $str = "118"; }
	else if($origin== "MAN" && $dest == "BOS"){ $str = "121"; }
	else if($origin== "BOS" && $dest == "MAN"){ $str = "122"; }
	else if($origin== "MAN" && $dest == "JFK"){ $str = "127"; }
	else if($origin== "JFK" && $dest == "MAN"){ $str = "128"; }
	else if($origin== "LHR" && $dest == "BGI"){ $str = "131"; }
	else if($origin== "BGI" && $dest == "LHR"){ $str = "132"; }
	else if($origin== "LHR" && $dest == "JFK"){ $str = "137"; }
	else if($origin== "JFK" && $dest == "LHR"){ $str = "138"; }
	else if($origin== "LHR" && $dest == "LAS"){ $str = "151"; }
	else if($origin== "LAS" && $dest == "LHR"){ $str = "152"; }
	else if($origin== "LHR" && $dest == "JFK"){ $str = "153"; }
	else if($origin== "JFK" && $dest == "LHR"){ $str = "154"; }
	else if($origin== "LHR" && $dest == "LAS"){ $str = "155"; }
	else if($origin== "LAS" && $dest == "LHR"){ $str = "156"; }
	else if($origin== "LHR" && $dest == "BOS"){ $str = "157"; }
	else if($origin== "BOS" && $dest == "LHR"){ $str = "158"; }
	else if($origin== "BFS" && $dest == "MCO"){ $str = "161"; }
	else if($origin== "MCO" && $dest == "BFS"){ $str = "162"; }
	else if($origin== "MAN" && $dest == "LAX"){ $str = "181"; }
	else if($origin== "LAX" && $dest == "MAN"){ $str = "182"; }
	else if($origin== "LHR" && $dest == "HKG"){ $str = "206"; }
	else if($origin== "HKG" && $dest == "LHR"){ $str = "207"; }
	else if($origin== "LHR" && $dest == "PVG"){ $str = "250"; }
	else if($origin== "PVG" && $dest == "LHR"){ $str = "251"; }
	else if($origin== "LHR" && $dest == "DEL"){ $str = "300"; }
	else if($origin== "DEL" && $dest == "LHR"){ $str = "301"; }
	else if($origin== "LHR" && $dest == "BOM"){ $str = "354"; }
	else if($origin== "BOM" && $dest == "LHR"){ $str = "355"; }
	else if($origin== "LHR" && $dest == "LOS"){ $str = "411"; }
	else if($origin== "LOS" && $dest == "LHR"){ $str = "412"; }
	else if($origin== "LHR" && $dest == "JNB"){ $str = "449"; }
	else if($origin== "JNB" && $dest == "LHR"){ $str = "450"; }
	else if($origin== "LHR" && $dest == "TLV"){ $str = "453"; }
	else if($origin== "TLV" && $dest == "LHR"){ $str = "454"; }
	else if($origin== "LHR" && $dest == "JNB"){ $str = "461"; }
	else if($origin== "JNB" && $dest == "LHR"){ $str = "462"; }
	else if($origin== "LHR" && $dest == "GRU"){ $str = "197"; }
	else if($origin== "GRU" && $dest == "LHR"){ $str = "198"; }
	
	$str = "VS:" . $str;
	
	return $str;
}

function container_link($trip_type, $cabin){

	$linkLocation = "";

	if($trip_type === "Flight" && $cabin != "Economy Delight"){
		
		$linkLocation = "FlexiCal";
		
	} elseif($trip_type === "Flight + Hotel"){
		
		$linkLocation = "TravelPlus";
		
	} elseif($trip_type === "Flight" && $cabin === "Economy Delight"){
		
		$linkLocation = "SearchResults";
		
	}

	return $linkLocation;

}

function generateMonthsArr($monthsstr){

	$monthsArr = [];

	if(!trim($monthsstr) == ""){
		//echo "EMPTYSTR";
		$monthsArr = explode(",", $monthsstr);
	}
	//$monthsArr = explode(",", $monthsstr);
	//$monthsArr = array_reverse($monthsArr);

	return $monthsArr;

}

function monthNumber($monthstr){
	$val = "";
	$monthstr = strtolower($monthstr);

	//January,February,March,April,May,June,July,August,September,October,November,December
	if($monthstr == 'january'){ $val = "1"; }
	elseif($monthstr == 'february'){ $val = "2"; }
	elseif($monthstr == 'march'){ $val = "3"; }
	elseif($monthstr == 'april'){ $val = "4"; }
	elseif($monthstr == 'may'){ $val = "5"; }
	elseif($monthstr == 'june'){ $val = "6"; }
	elseif($monthstr == 'july'){ $val = "7"; }
	elseif($monthstr == 'august'){ $val = "8"; }
	elseif($monthstr == 'september'){ $val = "9"; }
	elseif($monthstr == 'october'){ $val = "10"; }
	elseif($monthstr == 'november'){ $val = "11"; }
	elseif($monthstr == 'december'){ $val = "12"; }

	return $val;

}

function tpMonthNumber($monthstr){
	$val = "";
	$monthstr = strtolower($monthstr);

	//January,February,March,April,May,June,July,August,September,October,November,December
	if($monthstr == 'january'){ $val = "01"; }
	elseif($monthstr == 'february'){ $val = "02"; }
	elseif($monthstr == 'march'){ $val = "03"; }
	elseif($monthstr == 'april'){ $val = "04"; }
	elseif($monthstr == 'may'){ $val = "05"; }
	elseif($monthstr == 'june'){ $val = "06"; }
	elseif($monthstr == 'july'){ $val = "07"; }
	elseif($monthstr == 'august'){ $val = "08"; }
	elseif($monthstr == 'september'){ $val = "09"; }
	elseif($monthstr == 'october'){ $val = "10"; }
	elseif($monthstr == 'november'){ $val = "11"; }
	elseif($monthstr == 'december'){ $val = "12"; }

	return $val;

}

function getMonth($date){
	$month = '';
	//echo $date;
	$val = date_parse_from_format("d/m/Y", $date);
	//var_dump($val);
	$val = $val["month"];
	//echo "MONTHHERE". $val;
	if($val == '01' ){ $month = 'January'; }
	elseif($val == '02' ){ $month = 'February'; }
	elseif($val == '03' ){ $month = 'March'; }
	elseif($val == '04' ){ $month = 'April'; }
	elseif($val == '05' ){ $month = 'May'; }
	elseif($val == '06' ){ $month = 'June'; }
	elseif($val == '07' ){ $month = 'July'; }
	elseif($val == '08' ){ $month = 'August'; }
	elseif($val == '09' ){ $month = 'September'; }
	elseif($val == '10' ){ $month = 'October'; }
	elseif($val == '11' ){ $month = 'November'; }
	elseif($val == '12' ){ $month = 'December'; }
	
	return $month;
}



function travelplusDestName($code){
	
	$destination = '';
	
	if($code == "ANU"){ $destination = 'antigua'; }
	else if($code == "ATL"){ $destination = 'atlanta'; }
	else if($code == "BOS"){ $destination = 'boston'; }
	else if($code == "BGI"){ $destination = 'barbados'; }
	else if($code == "HAV"){ $destination = 'cuba'; }
	else if($code == "DEL"){ $destination = 'delhi'; }
	else if($code == "DXB"){ $destination = 'dubai-beaches-and-beyond'; }
	else if($code == "GND"){ $destination = 'grenada'; }
	else if($code == "HKG"){ $destination = 'hong-kong'; }
	else if($code == "JNB"){ $destination = 'johannesburg'; }
	else if($code == "LOS"){ $destination = 'africa'; }
	else if($code == "LAS"){ $destination = 'las-vegas'; }
	else if($code == "LAX"){ $destination = 'los-angeles'; }
	else if($code == "MIA"){ $destination = 'miami'; }
	else if($code == "MBJ"){ $destination = 'jamaica'; }
	else if($code == "JFK"){ $destination = 'new-york'; }
	else if($code == "MCO"){ $destination = 'orlando'; }
	else if($code == "SFO"){ $destination = 'san-francisco'; }
	else if($code == "SEA"){ $destination = 'seattle'; }
	else if($code == "PVG"){ $destination = 'shanghai'; }
	else if($code == "UVF"){ $destination = 'st-lucia'; }
	else if($code == "TAB"){ $destination = 'tobago'; }
	else if($code == "IAD"){ $destination = 'washington-dc'; }
	else if($code == "TLV"){ $destination = 'tel-aviv'; }
	else if($code == "BOM"){ $destination = 'mumbai'; }
	else if($code == "GRU"){ $destination = 'sao-paulo'; }
	
	return $destination;
}

function removeSpaces($str){
	$string = '';
	$string = str_replace(' ', '', $str);
	//$string = str_replace('\'', '', $string);
	$string = strtoupper($string);
	return $string;
	
}

function addDashes($str){
	$string = '';
	$string = str_replace(' ', '-', $str);
	$string = str_replace('\'', '', $string);
	$string = str_replace(',', '', $string);
	$string = strtoupper($string);
	return $string;
	
}

function formatdatefortracking($str,$type){
	$d = strtotime($str);
	
	if($type == 'Flight'){
		//eg. 08 Jan 20
		$formatted_date = date("dMy", $d);
	} else if($type == 'Flight + Hotel'){
		$formatted_date = date("Mdy", $d);
	}
	
	return $formatted_date;
}

function datesFlexible($cabin){
	
	$str = "true";
	
	if($cabin == "Economy Delight"){
		$str = "undefined";
	}
	
	return $str;
	
}


?>