<?php
require __DIR__ . '/vendor/autoload.php';

$client = new \Google_Client();
$client->setApplicationName('Google sheets and php');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__ . '/VirginAtlantic_2019-6427cb528508.json');

$service = new Google_Service_Sheets($client);
$spreadsheetID = '19BF-5i73qFG1Uh900zgJtpkLAb3H8fe6pPp1mNFJfJo'; // LIVE Spreadsheet ID
// $spreadsheetID = '1XvcwNtel52X_GYT65Gij2VFAfq7j3R4v0UNfNUWRU6U'; // Staging Spreadsheet ID

//$range = "sheet1!A2:AC9";
$range = "sheet1!2:100";
$response = $service->spreadsheets_values->get($spreadsheetID, $range);
$values = $response->getValues();

if(empty($values)){
	print "No data found";
} else {
	$mask = "%10s %-10s %s\n";
	
	//main routes array
	$routes_array = [];
	$destinations_array = [];
	
	foreach($values as $row){
		//$ob = json_decode($row);
		//var_dump($row);
		$image = $row[0];
		$trip_type = $row[1];
		//$origin_visual = $row[3];
		$origin = $row[2];
		//$destination = $row[4];
		//$hotel_ID = $row[4];
		$hotel_name = $row[3];
		$star_rating = $row[4];
		$destination_code = $row[5];
		//$data_destination = $row[9];
		//$destination_code_formatted = $row[7];
		$cabin_visual = $row[6];
		//$cabin_formatted = $row[12];
		//$outbound_date_visual = $row[13];
		//$flight_outbound_date_visual = $row[14];
		$NEW_OUTBOUND_DATE = $row[7];
		$NEW_INBOUND_DATE = $row[8];
		//echo "DATEHERE" . $NEW_OUTBOUND_DATE;
		//$inbound_date_visual = $row[15];
		//$outbound_flight_formatted = $row[12];
		//$outbound_date_formatted = $row[17];
		//$inbound_flight_formatted = $row[13];
		//$inbound_date_formatted = $row[19];
		$duration = $row[9];
		//$container_link = $row[12];
		$ss_price = $row[10];
		//$promo_box = $row[11];
		$promo_message = $row[11];
		$saving = $row[12];
		//$hide_months = $row[16];
		$months = $row[13];
		$show_on_homepage = $row[14];
		$red_tab = $row[15];
		$show_on_homepage_upper = $row[16];
		$show_on_last_minute_deals = $row[17];
		
		
		//current date plus 7 days - day-month-year
		$departureDate = date("d-m-Y", time() + 604800);
		
		
		$obj = new stdClass;
		$obj->image = $image;
		$obj->trip_type = $trip_type;
		$obj->origin_visual = origin_visual($origin);
		$obj->origin_code = $origin;
		$obj->destination_visual = destination_visual($destination_code);
		//$obj->hotel_ID = $hotel_ID;
		$obj->hotel_name = $hotel_name;
		
		$obj->star_rating = $star_rating;
		//$obj->destination_code_visual = $destination_code_visual;
		$obj->destination_code = $destination_code;
		//$obj->data_destination = $data_destination;
		//$obj->destination_code_formatted = $destination_code_formatted;
		$obj->destination_code_formatted = destination_code_formatted($destination_code);
		$obj->cabin_visual = $cabin_visual;
		//$obj->cabin_formatted = $cabin_formatted;
		$obj->cabin_formatted = cabin_formatted($cabin_visual);
		$obj->outbound_date_visual = outbound_date_visual($NEW_OUTBOUND_DATE);
		$obj->flight_outbound_date_visual = flight_outbound_date_visual($NEW_OUTBOUND_DATE);
		$obj->NEW_OUTBOUND_DATE = $NEW_OUTBOUND_DATE;
		$obj->NEW_INBOUND_DATE = $NEW_INBOUND_DATE;
		$obj->inbound_date_visual = inbound_date_visual($NEW_INBOUND_DATE);
		//$obj->outbound_flight_formatted = $outbound_flight_formatted;
		$obj->outbound_flight_formatted = flight_formatted($origin, $destination_code);
		$obj->outbound_date_formatted = outbound_date_formatted($trip_type, $NEW_OUTBOUND_DATE);
		$obj->inbound_flight_formatted = flight_formatted($destination_code, $origin);
		//$obj->inbound_date_formatted = $inbound_date_formatted;
		$obj->inbound_date_formatted = inbound_date_formatted($trip_type, $NEW_INBOUND_DATE);
		$obj->duration = $duration;
		//$obj->container_link = $container_link;
		$obj->container_link = container_link($trip_type, $cabin_visual);
		$obj->ss_price = $ss_price;
		//$obj->promo_box = $promo_box;
		$obj->promo_message = $promo_message;
		$obj->saving = $saving;
		//$obj->hide_months = $hide_months;
		$obj->months = generateMonthsArr($months);
		$obj->show_on_homepage = $show_on_homepage;
		$obj->show_on_homepage_upper = $show_on_homepage_upper;
		$obj->show_on_last_minute_deals = $show_on_last_minute_deals;
		$obj->red_tab = $red_tab;
		$obj->flightStatus = flightStatus($trip_type);
		$obj->flightClass = flightClass($cabin_visual);
		$obj->travelplusDestName = travelplusDestName($destination_code);

		array_push($routes_array, $obj);
		array_push($destinations_array, getRegionFromDestination($obj->destination_code_formatted));
		$destination_array_clean = array_unique($destinations_array);
		$destination_array_clean = array_filter($destination_array_clean, function($value) { return !is_null($value) && $value !== ''; });
		$regions_array = array_map("formatRegionObject", $destination_array_clean);
	}
}

?>