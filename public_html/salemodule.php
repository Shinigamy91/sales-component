<div class="container WS-flight-container" 
	data-flightlink="<?php echo generateLink($route, $rowNumber); ?>"
	data-depflightcode="<?php echo $route->origin_code ?>"
	data-desflightcode="<?php echo $route->destination_code; ?>"
	data-flightstatus="<?php echo $route->flightStatus; ?>"
	data-flightclass="<?php echo $route->flightClass ?>">

	<a href="<?php echo generateLink($route, $rowNumber); ?>" target="_blank">

		<?php if (!empty($route->red_tab)) { ?>
			<div class="red-tab-cont">
				<div class="red-tab"><?php echo $route->red_tab; ?></div>
			</div>
		<?php } ?>
		
		<div class="WS-flight-detail">
			<div class="WS-loc-img"><img src="<?php echo $route->image; ?>" alt="<?php echo $route->destination_visual; ?>"></div>

			<div class="WS-flight-location">

				<div class="WS-flight-type">
					<?php if ($route->trip_type == 'Flight + Hotel') { ?> 
						<span class="flighttype fltHotel flight-hotel">Flight + Hotel</span>
					<?php } ?>
					
					<?php if ($route->cabin_visual == 'Economy Light') { ?> 
						<span class="flighttype flight-economy-light"><?php echo $route->cabin_visual; ?></span>
					<?php } ?>

					<?php if ($route->cabin_visual == 'Economy Delight') { ?> 
						<span class="flighttype flight-economy-light"><?php echo $route->cabin_visual; ?></span>
					<?php } ?>

					<?php if ($route->cabin_visual == 'Economy Classic') { ?> 
						<span class="flighttype flight-economy-light"><?php echo $route->cabin_visual; ?></span>
					<?php } ?>

					<?php if ($route->cabin_visual == 'Premium') { ?> 
						<span class="flighttype flight-premium"><?php echo $route->cabin_visual; ?></span>
					<?php } ?>

					<?php if ($route->cabin_visual == 'Upper Class') { ?> 
						<span class="flighttype flight-upper-class"><?php echo $route->cabin_visual; ?></span>
					<?php } ?>
				</div>

				<?php if($route->trip_type == 'Flight') { ?>
					<div class="WS-location-point">
						<div class="loc loc-departure">
							<span class="depFlight"><?php echo $route->origin_visual; ?></span>
							<span class="flightCode">(<?php echo $route->origin_code ?>)</span>
						</div>
						<span class="dubbleArrow"></span>
						<div class="loc loc-arrival">
							<span class="destFlight"><?php echo $route->destination_visual; ?></span>
							<span class="flightCode">(<?php echo $route->destination_code; ?>)</span>
						</div>
					</div>
				<?php } elseif ($route->trip_type == 'Flight + Hotel') {  ?>
					<div class="WS-location-point">
						<div class="loc loc-departure">
							<span class="depFlight"><?php echo $route->origin_visual; ?></span>
							<span class="flightCode">(<?php echo $route->origin_code ?>)</span>
						</div>
						<span class="dubbleArrow"></span>
						<div class="loc loc-arrival">
							<span class="destFlight"><?php echo $route->destination_visual; ?></span>
							<span class="flightCode">(<?php echo $route->destination_code; ?>)</span>
						</div>
						<div class="WS-dest-hotel">
							<span class="destHotel"><?php echo $route->hotel_name; ?></span>
							<div class="star five rating">
								<?php echo stars($route->star_rating) ?>
							</div>
						</div>
					</div>
				
				<?php } ?>
				
				<div class="WS-location-detail">

					<?php if ($route->trip_type == 'Flight') {  ?>
						<div class="flights-info-row">
							<div class="WS-loc-txt">
								<span class="WS-loc-icn">
									<img src="https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Calendar.svg" style="width: 16px;">
								</span>

								<span class="WS-outbound-date"><?php echo $route->flight_outbound_date_visual; ?></span>
								- 
								<span class="WS-return-date"><?php echo $route->inbound_date_visual; ?></span>
							</div>
						</div>
						<div class="flights-info-row">
							<?php echo cabinVisual( $route->cabin_visual) ?>
						</div>
					<?php } ?>

					<?php if ($route->trip_type == 'Flight + Hotel') {  ?>
						<div class="flights-hotel-info-row">
							<div class="WS-loc-txt">
								<span class="WS-loc-icn">
									<img src="https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/Calendar.svg" style="width: 16px;">
								</span>

								<span class="WS-outbound-date"><?php echo $route->outbound_date_visual; ?></span>
								- 
								<span class="WS-return-date"><?php echo $route->inbound_date_visual; ?></span>
							</div>
							<div class="WS-loc-txt">
								<span class="WS-loc-icn WS-nights-icn">
									<img src="https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/nights.svg" style="width: 16px;">
								</span>
								<span class="WS-nights"><?php echo $route->duration; ?> nights</span>
							</div>
						</div>

						<div class="flights-hotel-info-row">
							<div class="WS-loc-txt">
								<span class="WS-loc-icn">
									<img src="https://content.virginatlantic.com/content/dam/vaa/Site%20Imagery/campaigns/winter/My%20account.svg" style="width: 16px;">
								</span>
								2 Adults
							</div>

							<?php echo cabinVisual( $route->cabin_visual) ?>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="ES-price-container">
				<?php if ($route->saving != '') { ?>
					<div class="addOffer">
						<span class="WS-booking-offer">Save £<?php echo $route->saving; ?></span>
						<span class="WS-was-price">per booking</span>
					</div>
				<?php } ?>
				
				<div class="WS-booking-price <?php if ($route->saving != '') { ?>ES-offer<?php } ?>">
					<span class="WS-booking-now">Book for </span>£<?php echo number_format($route->ss_price); ?><sup>pp</sup>
				</div>
				
				<?php if (!empty($route->promo_message)) { ?>
					<div class="secure-container-text">
						<span class="secure-your-flight"><?php echo $route->promo_message; ?></span>	
					</div>
				<?php } ?>
			</div>
	
		</div>

	</a>

	<?php if (!empty($route->months)) { ?>

		<div class="WS-find-other-date-open">
			<div class="WS-find-other-date-open-content">
				<?php foreach($route->months as $key => $date){ ?>
					<?php if( $route->trip_type == 'Flight'){ ?>
					<span><a href="https://www.virginatlantic.com/air-shopping/findFlights.action?action=findFlights&tripType=ROUND_TRIP&priceSchedule=PRICE&originCity=<?php echo $route->origin_code; ?>&destinationCity=<?php echo $route->destination_code_formatted; ?>&departureDate=<?php echo outbound_date_formatted($route->trip_type, $date) ?>&departureTime=AT&returnDate=<?php echo addSevenDays($date) ?>&returnTime=AT&paxCount=1&searchByCabin=true&cabinFareClass=<?php echo $route->cabin_formatted ?>&deltaOnlySearch=false&deltaOnly=off&Go=Find%20Flights&meetingEventCode=&refundableFlightsOnly=false&compareAirport=false&awardTravel=false&datesFlexible=<?php echo datesFlexible($route->cabin_visual) ?>&flexAirport=false&paxCounts[0]=2&paxCounts[1]=0&paxCounts[2]=0&paxCounts[3]=0<?php //echo genTrackingLink($route, $rowNumber, $date) ?>"><?php echo getMonth($date) ?><b></b></a></span>
					<?php } else if( $route->trip_type == 'Flight + Hotel'){ ?>
					<span><a href="https://travelplus.virginatlantic.com/flight+hotel/<?php echo $route->travelplusDestName ?>?departureDate=<?php echo outbound_date_formatted($route->trip_type, $date) ?>&duration=<?php echo $route->duration ?>&gateway=<?php echo $route->origin_code ?>&room=a2,i0<?php echo genTrackingLink($route, $rowNumber, $date) ?>"><?php echo getMonth($date) ?><b></b></a></span>
					<?php } ?>
				<?php } ?>
			</div>
		</div>

		<div class="WS-find-other-date-clossed">
			<span class="WS-find-other-date-txt">Find other dates</span>
			<span class="WS-find-other-date-icn"></span>
		</div>

	<?php } ?>
</div>
