<?php
	header("Content-type: text; charset=utf-8");
	header('Access-Control-Allow-Origin: *');
?>
<link type="text/css">
<style>
.winter-sale-wrapper .WS-booking-price,.winter-sale-wrapper .WS-loc-txt,.winter-sale-wrapper .depFlight,.winter-sale-wrapper .destFlight,.winter-sale-wrapper .flightCode{font-weight:500;line-height:normal;letter-spacing:normal;font-stretch:normal;font-style:normal}.winter-sale-wrapper{float:none;width:100%;max-width:1180px;margin:auto}.WS-flight-detail{float:left;width:100%}.winter-sale-wrapper{margin-bottom:60px;display:flex;flex-wrap:wrap}.WS-booking-price{float:right}.winter-sale-wrapper .depFlight,.winter-sale-wrapper .destFlight{font-family:gotham-medium;font-size:22px;color:#313131}.winter-sale-wrapper .WS-loc-txt,.winter-sale-wrapper .flightCode{font-family:gotham-book;font-size:14px}.winter-sale-wrapper .flightCode{color:#a8a8a8;margin-left:10px}.winter-sale-wrapper .WS-loc-txt{color:#222;margin-right:15px;line-height:25px;display:inline-block;margin-bottom:15px}.winter-sale-wrapper .WS-loc-icn img{width:30px;margin-top:-3px;margin-right:7px}.winter-sale-wrapper .dubbleArrow{background:url(https://content.virginatlantic.com/content/dam/virgin-applications/fresh-air-core/1.0.18/images/Arrow_2.svg) 50% no-repeat!important;display:inline-block;height:27px;width:27px;top:3px;position:relative;margin-right:10px;margin-left:10px;zoom:.7}.winter-sale-wrapper .WS-loc-img{float:left}.winter-sale-wrapper .WS-loc-img img{width:130px;height:134px;margin-right:20px}.winter-sale-wrapper .WS-flight-detail{border:1px solid #f3f3f3}.winter-sale-wrapper .WS-location-detail{float:left}.winter-sale-wrapper .WS-flight-location{float:left;padding-top:20px;width:60%}.winter-sale-wrapper .WS-booking-price-link:before,.winter-sale-wrapper .WS-find-other-date-icn:before{font-style:normal;line-height:1;height:100%;margin-left:10px;display:inline-block;content:"\E90D"!important;font-variant:normal;text-transform:none;position:relative}.winter-sale-wrapper .WS-location-point{margin-bottom:13px}.winter-sale-wrapper .WS-booking-price-link{top:-7px;display:inline-block;position:relative}.winter-sale-wrapper .WS-find-other-date-txt{font-family:Gotham-book;font-size:12px;font-weight:500;color:#4f145b;text-decoration:underline}.winter-sale-wrapper .WS-find-other-date-icn:before{color:#4f145b;font-family:icomoon!important;font-weight:400;font-size:6px;-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0)}.winter-sale-wrapper .WS-find-other-date-icn-active:before{-ms-transform:rotate(180deg);-webkit-transform:rotate(180deg);transform:rotate(180deg)}.winter-sale-wrapper .WS-find-other-date-icn{top:-1px;position:relative}.winter-sale-wrapper .WS-find-other-date-clossed{float:left;width:100%;text-align:center;padding:8px 0;cursor:pointer;background-color:#f3f3f3}.winter-sale-wrapper .WS-booking-now,.winter-sale-wrapper .WS-find-other-date-open-content span{font-weight:500;letter-spacing:normal;text-align:right;font-family:Gotham-book;font-stretch:normal;font-style:normal}.winter-sale-wrapper .WS-booking-now{font-size:12px;line-height:normal;color:#e1163c;width:50px;display:inline-block;margin-right:5px}.winter-sale-wrapper .WS-booking-price sup{font-size:16px;color:#e1163c}.winter-sale-wrapper .WS-find-other-date-open-content span{font-size:14px;color:#222;margin-right:33px;float:left;line-height:30px}.winter-sale-wrapper .WS-find-other-date-open-content span a{color:#222;text-decoration:underline}.winter-sale-wrapper .WS-find-other-date-open-content span sup{font-size:14px;top:0;color:#222}.winter-sale-wrapper .WS-find-other-date-open-content span.activeDate a,.winter-sale-wrapper .WS-find-other-date-open-content span.activeDate sup{color:#e1163c!important}.targetWinterSale{display:none}.winter-sale-wrapper .WS-find-other-date-open{float:left;width:100%;border:1px solid #f3f3f3;border-top:0;padding:20px;display:none}.winter-sale-wrapper .WS-filter-container{float:left;width:100%;margin-bottom:40px}.winter-sale-wrapper .WS-flight-anywhere,.winter-sale-wrapper .WS-flight-class,.winter-sale-wrapper .WS-flight-or-hotel,.winter-sale-wrapper .WS-flight-station{float:left;margin-right:1.7%;width:23.3%}.winter-sale-wrapper .WS-flight-class{margin-right:0!important}.winter-sale-wrapper .WS-flight-anywhere select,.winter-sale-wrapper .WS-flight-class select,.winter-sale-wrapper .WS-flight-or-hotel select,.winter-sale-wrapper .WS-flight-station select{width:100%;background:#fff;font-family:Gotham-book;font-size:16px;font-weight:400;font-style:normal;font-stretch:normal;line-height:1.5;letter-spacing:.1px;color:#601a69;height:50px;text-align:left;border-color:#f3f3f3;border-bottom:1px solid #d8d7d7}.winter-sale-wrapper .WS-flight-anywhere:after,.winter-sale-wrapper .WS-flight-class:after,.winter-sale-wrapper .WS-flight-or-hotel:after,.winter-sale-wrapper .WS-flight-station:after{color:#d31641;content:"\E90D"!important;font-family:icomoon!important;font-style:normal;font-weight:700;font-variant:normal;line-height:1;text-transform:none;font-size:6px;position:relative;height:100%;-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0);display:inline-block;top:-33px;right:-90%}.winter-sale-wrapper .loadMore{float:left;width:100%;text-align:center;font-family:gotham-light;color:#61126b;font-weight:700;font-size:16px;cursor:pointer}.loadMore:after{color:#61126b;font-family:icomoon!important;font-weight:400;font-size:7px!important;-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0)}#content>div.main.parsys.removeFocus>div.descriptivetext.section.removeFocus>div,.hovernavigation,.pagetabscomponent,.rtecomponent,.textandasset{display:none}.WS-flight-container{cursor:pointer;margin-bottom:30px}.WS-flight-detail.WS-flight-hotel{margin-bottom:25px}.breadCrumbsComp .breadcrumb{width:75%}.winter-sale-wrapper .WS-flight-hotel .WS-location-detail{width:60%!important;float:left}.WS-flight-container{display:none}.addoffer{display:none}.WS-booking-price .WS-booking-offer{border:1px solid #61126b;border-bottom:2px solid;font-size:10px;padding:3px 8px;line-height:12px;color:#61126b;font-weight:500;display:block;width:90px;float:right}.responsive .heroCompImg .imgTxtFullWidth{left:40%!important}.ES-price-container{width:220px;display:block;float:right;font-size:1rem;margin-top:1.5em}.winter-sale-wrapper .WS-flight-detail{padding:25px 25px 5px}.winter-sale-wrapper .WS-booking-price{float:right;padding:16px 18px!important;font-family:gotham-medium;text-align:center;width:100%;border:none;color:#fff;background:#e1163c;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;cursor:pointer;font-weight:600}.winter-sale-wrapper .WS-booking-price.ES-offer{border-radius:0 0 4px 4px;margin-top:0}.WS-booking-price sup{color:#fff!important;top:0!important}.winter-sale-wrapper .WS-flight-location{padding-top:0;width:55%!important}.winter-sale-wrapper .WS-booking-now{font-size:18px;line-height:22px;color:#fff;width:auto;display:inline-block;margin-right:5px;font-family:gotham-light}.WS-dest-hotel span.WS-loc-icn{margin-right:7px}.WS-nights-icn{margin-left:10px}.winter-sale-wrapper .WS-dest-hotel{margin-top:12px;overflow:hidden}.winter-sale-wrapper .destHotel{margin-right:10px;font-size:22px;line-height:26px;margin-bottom:5px;color:#313131;font-family:Gotham-medium;float:left}.winter-sale-wrapper .WS-booking-price-link{top:-4px!important}.svgflag .triangle{transform:rotate(-180deg);background-color:red;fill:green;stroke:green;stroke-width:4;transition:all .8s ease-in-out;transform-origin:15px 15px}.addOffer{float:right;color:#4d4d4d;font-family:gotham-medium;width:100%;height:3.5em;background-color:#f9f9f9;border-radius:4px 4px 0 0;border:1px solid #ccc;text-align:center;vertical-align:middle;padding:.7em 2.6em}.secure-your-flight{height:36px;color:#757575;font-family:Gotham-book;font-size:12px;font-weight:500;letter-spacing:.01em;line-height:18px}.secure-container-text{width:100%;text-align:center;margin:1em 0;position:relative;float:right}.starClass-0{fill:#c11447}.starClass-1{fill:#d9d9d9}.container.WS-flight-container.WS-zero-results{text-align:center;cursor:default}span.WS-zero-results-title{display:block;color:#e1193e;font-family:Gotham-medium;font-size:1.8em;margin-bottom:15px}span.WS-zero-results-text{display:block;font-family:Gotham-light;font-size:1.25em;font-weight:400}.WS-find-other-date-clossed.WS-zero-results-links span{display:inline-block}.WS-find-other-date-clossed.WS-zero-results-links a{font-size:1.3em;font-family:Gotham-medium;margin-right:30px}.WS-zero-results-links a span{background:url(https://www.virginatlantic.com/content/dam/virgin-applications/images/staticpages/sprites/widget_panel_sprite.png) 0 -1113px no-repeat;width:20px;height:13px;margin:0 0 0 15px;display:inline-block}.WS-find-other-date-clossed.WS-zero-results-links{cursor:default}.winter-sale-wrapper .loadMore{margin-bottom:25px}span.WS-booking-offer{position:relative;margin:6px 6px;font-size:1em}.WS-was-price{display:block;font-size:.8em}.red-tab-cont{display:block;text-align:right}.red-tab{display:inline-block;padding:7px 11px;border:solid 1px #d3d3d3;background-color:#ddd;color:#61126b;border-bottom:none;font-family:gotham-book;font-size:12px;height:30px;width:auto}.flighttype{font-size:12px;line-height:14px;display:inline-block;margin-right:15px;padding:4px 15px;font-family:Gotham-book}.flight-hotel{color:#222;border:1px solid #222}.flight-economy-light{color:#eb143a;border:1px solid #eb143a}.flight-premium{background:#222;color:#fff}.flight-upper-class{background:#4f145b;color:#fff}.WS-flight-type{margin-bottom:8px}.rating.star{float:left;overflow:hidden;margin-top:8px}@media only screen and (max-width :769px){.winter-sale-wrapper .WS-flight-detail{padding-bottom:25px}}@media only screen and (max-width :992px){.winter-sale-wrapper .WS-flight-anywhere,.winter-sale-wrapper .WS-flight-class,.winter-sale-wrapper .WS-flight-or-hotel,.winter-sale-wrapper .WS-flight-station{margin-right:2%;width:48%}.container.WS-flight-container{display:flex;width:46%!important;padding-right:0!important;padding-left:0!important;float:left!important;margin-left:2%!important;margin-right:2%!important;border:1px solid #f3f3f3;flex-direction:column;justify-content:space-between;margin-bottom:20px}.winter-sale-wrapper .WS-flight-detail{border:none}.winter-sale-wrapper .WS-find-other-date-clossed{margin-bottom:0;border-top:1px solid #f3f3f3}.winter-sale-wrapper .depFlight,.winter-sale-wrapper .destFlight{display:inline-block}.winter-sale-wrapper .WS-flight-anywhere:after,.winter-sale-wrapper .WS-flight-class:after,.winter-sale-wrapper .WS-flight-or-hotel:after,.winter-sale-wrapper .WS-flight-station:after{left:90%!important}.winter-sale-wrapper .WS-location-detail{width:100%}span.WS-outbound-date{display:inline-block}.winter-sale-wrapper .WS-loc-txt,span.WS-nights{display:block;margin-bottom:15px}span.WS-loc-icn{float:left}.winter-sale-wrapper .WS-loc-img{width:100%}.winter-sale-wrapper .WS-loc-img img{width:100%;height:auto}.winter-sale-wrapper .depFlight{margin-bottom:5px}.winter-sale-wrapper .WS-flight-location{width:100%!important}.winter-sale-wrapper .WS-booking-price{width:100%;text-align:center}.secure-container-text{width:100%}.winter-sale-wrapper .dubbleArrow{display:none}.winter-sale-wrapper .WS-location-point{margin-bottom:15px}.winter-sale-wrapper .WS-dest-hotel{margin-top:20px;overflow:hidden}.winter-sale-wrapper .destHotel{display:block}.WS-dest-hotel span.WS-loc-icn{float:left;margin-right:0}.winter-sale-wrapper .destHotel{font-size:22px;line-height:26px;margin-bottom:12px;color:#313131;font-family:Gotham-medium;float:left}div.star{display:inline-block}.secure-container-text{text-align:center}div.ES-price-container{width:100%;margin-top:20px}.WS-nights-icn{margin-left:0;clear:left}.flighttype{margin-top:15px}}.loc{display:inline-block}@media only screen and (max-width :992px){div.heroCompWrapper a.whiteButton_tt{width:100%;padding:12px 0!important;text-align:center!important}}@media only screen and (max-width :769px){.loc{display:block;width:100%}}@media only screen and (max-width :750px){.responsive .heroCompImg .imgTxtFullWidth{left:0!important}}@media only screen and (max-width :576px){.winter-sale-wrapper .WS-flight-anywhere,.winter-sale-wrapper .WS-flight-class,.winter-sale-wrapper .WS-flight-or-hotel,.winter-sale-wrapper .WS-flight-station{margin-right:0;width:100%}.container.WS-flight-container{width:100%!important;float:none!important;margin-right:auto!important;margin-left:auto!important}.responsive .heroCompImg .fullWidthImg{margin-top:40px!important}}@media only screen and (max-width :415px){.container.WS-flight-container{margin-right:15px!important;margin-left:15px!important}}
</style>

<?php
set_time_limit(0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	
require('functions.php');
require('googledoc-prod.php');
?>

<div class="winter-sale-wrapper fresh-air targetWinterSale">
	<div class="container">
		<div class="WS-filter-container">
			<div class="WS-flight-station">
				<select>
					<option value="origin">From all UK airports</option>
					<option value="LGW">London Gatwick(LGW)</option>
					<option value="LHR">London Heathrow(LHR)</option>
					<option value="MAN">Manchester(MAN)</option>
					<option value="BFS">Belfast(BFS)</option>
					<option value="GLA">Glasgow(GLA)</option>
				</select>
			</div>
			<div class="WS-flight-anywhere">
				<select>
                    <option value="region">To all regions</option>
                    <?php
						foreach ($regions_array as $rowNumberNew => $region) {
							echo '<option value="', $region->code, '">', $region->formatted, '</option>';
						}
					?>
				</select>
			</div>
			<div class="WS-flight-or-hotel">
				<select>
					<option value="triptype">All trip types</option>
					<option value="flight">Flight only</option>
					<option value="flighthotel">Flight + Hotel</option>
				</select>
			</div>
			<div class="WS-flight-class">
				<select>
                    <option value="cabinclass">All cabin types</option>
					<option value="economy-classic">Economy Classic</option>
					<option value="economy-light">Economy Light</option>
					<option value="economy-delight">Economy Delight</option>
					<option value="premium">Premium</option>
					<option value="upper">Upper Class</option>
				</select>
			</div>
		</div>
	</div>

<?php
	foreach ($routes_array as $rowNumber => $route) {
		include('salemodule.php');
	}
?>

<div class="loadMore">View more</div>
	
<div class="container WS-flight-container WS-zero-results" style="display: block;">
	<div class="WS-flight-detail" style="padding: 35px 25px;">
		<span class="WS-zero-results-title">Struggling to find the perfect escape?</span>
		<span class="WS-zero-results-text">Try tweaking our filters or start a fresh search via the links below.</span>
	</div>
	<div class="WS-find-other-date-clossed WS-zero-results-links" style="padding: 20px 0px;">
		<span>
			<a href="https://travelplus.virginatlantic.com/?cm_mmc=Virgin%20Atlantic-_-Main%20Navigation-_-Flight%20And%20Hotel-_-navvaa_1&utm_medium=referral&utm_source=virginatlantic.com&utm_campaign=homepage-link" target="_blank">Flight + Hotel<span></span></a>
		</span>
		<span>
			<a href="https://www.virginatlantic.com/air-shopping/searchFlights.action">Flight only<span></span></a>
		</span>
	</div>
</div>
	
	
</div>
<script>
(function () {
    var cnt = 10000;
    var paramsFiltered = false;

   	function init() {
		if (typeof (jQuery) !== 'undefined' && jQuery('.hero.heroimage').length > 0 && jQuery('.winter-sale-wrapper').length <= 1) {
			jQuery('.winter-sale-wrapper').insertAfter(jQuery('.hero.heroimage'));
			jQuery('.winter-sale-wrapper').removeClass('targetWinterSale');
			jQuery('.targetWinterSale').remove();
			
			jQuery('.WS-find-other-date-clossed').bind('click', function(){
				jQuery(this).parent().find('.WS-find-other-date-open').slideToggle('slow');
				jQuery(this).find('.WS-find-other-date-icn').toggleClass('WS-find-other-date-icn-active');
				
			});

			setFiltersEventListeners();

			//load more
			loadMore();
			
			getFilterParams();
			
			callOnceElLoaded(".alert-advisory-main-container", function(nav){
				console.log("Alert found");
				fixnavbar();
			});
		} else if (cnt > 0) {
			cnt = cnt - 500;
			setTimeout(init, 500);
		} else {
		}
    }
		
	function getUrlParameter(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		var results = regex.exec(location.search);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	};
		
	function getFilterParams(){
		var origin = getUrlParameter('origin');
		var region = getUrlParameter('region');
		var tripType = getUrlParameter('triptype');
		var cabinClass = getUrlParameter('cabin');
		
		if(origin != '' || region != '' || tripType != '' || cabinClass != ''){
			
			origin = (origin == '') ? "origin" : origin;
			region = (region == '') ? "region" : region;
			tripType = (tripType == '') ? "triptype" : tripType;
			cabinClass = (cabinClass == '') ? "cabinclass" : cabinClass;

			dataSort(origin, region, tripType, cabinClass);
		} else {
			paramsFiltered = true;
		}
    }
    
    function getDestinationsPerRegion(region) {
        var destinations = [];

        switch (region) {
            case 'caribbean':
                var destinations = ['ANU', 'BGI', 'GND', 'MBJ', 'UVF', 'TAB'];
                break;
            case 'asia':
                var destinations = ['HKG', 'SHA', 'BOM'];
                break;
            case 'west-coast':
                var destinations = ['LAS', 'LAX', 'SFO', 'SEA'];
                break;
            case 'africa-israel':
                var destinations = ['TLV', 'JNB', 'CPT', 'DUR'];
                break;
            case 'east-coast':
                var destinations = ['JFK', 'BOS', 'WAS', 'ATL', 'MCO', 'MIA'];
                break;
            default:
                var destinations = [];
        }

        return destinations;
    }

	function setFiltersEventListeners() {
		jQuery('.WS-filter-container select').on('change', function() {
			dataSort();
		});
	}
		
	function showZeroResults(){
		setTimeout(function(){
			//show zero results regardless
			$(".WS-flight-container.WS-zero-results").show();
			console.log("Show zero results");                 
        },500);
	}
		
	function dataSort(origin = "origin", destination = "region", tripType = "triptype", cabinClass = "cabinclass") {
		var flightclass, flightStatus, stationCode, desFlightCode;
		jQuery('.loadMore').hide();
		
		if (!paramsFiltered) {
			console.log("paramsfiltered false", paramsFiltered);
			flightclass = cabinClass;
			flightStatus = tripType;
			stationCode = origin;
			desFlightCode = destination;
			
			jQuery('.WS-flight-class select').val(flightclass);
			jQuery('.WS-flight-or-hotel select').val(flightStatus);
			jQuery('.WS-flight-station select').val(stationCode);
			jQuery('.WS-flight-anywhere select').val(desFlightCode);
			
			paramsFiltered = true;
		} else {
			console.log("paramsfiltered true");
			stationCode = jQuery('.WS-flight-station select').children("option:selected").val();
			desFlightCode = jQuery('.WS-flight-anywhere select').children("option:selected").val();
			flightStatus = jQuery('.WS-flight-or-hotel select').children("option:selected").val();
			flightclass = jQuery('.WS-flight-class select').children("option:selected").val();
        }

		// Filter through all items.
		jQuery('.WS-flight-container').each(function() {
			var canDisplay = true;
			var $this = jQuery(this);
			
			if (stationCode !== 'origin' && $this.data('depflightcode') !== stationCode) {
				canDisplay = false;
			}

			if (canDisplay && desFlightCode !== 'region') {
				var destinationsList = getDestinationsPerRegion(desFlightCode);

				var destinationIsInRegion = jQuery.inArray($this.data('desflightcode'), destinationsList);

				if (destinationIsInRegion === -1) {
					canDisplay = false;
				}
			}

			if (canDisplay && flightStatus !== 'triptype' && $this.data('flightstatus') !== flightStatus) {
				canDisplay = false;
			}

			if (canDisplay && flightclass !== 'cabinclass' && $this.data('flightclass') !== flightclass) {
				canDisplay = false;
			}

			if (canDisplay) {
				$this.fadeIn('10');
			} else {
				$this.fadeOut('10');
			}
		});

		showZeroResults();
    }

	function loadMore(){
		var size_li = jQuery(".winter-sale-wrapper .WS-flight-container").length;
		var x=20;
		jQuery('.winter-sale-wrapper .WS-flight-container:lt('+x+')').show();
		jQuery('.loadMore').click(function () {
			x= (x+20 <= size_li) ? x+20 : size_li;
			jQuery('.winter-sale-wrapper .WS-flight-container:lt('+x+')').show();
			if (x === size_li) {
				jQuery('.loadMore').hide();
			}
		});
    }

	function flightClickLink(){
		jQuery('.WS-flight-detail').bind('click', function(){
			window.location.href = jQuery(this).parent().data('flightlink');
		});
		jQuery('.WS-zero-results .WS-flight-detail').unbind("click");
    }

	function fixnavbar(){
		var nav = $("nav.navbar.fixed-top");
		var nav_cnt = 0;
		var navtop = setInterval(function(){
			var t = $(nav).css("top");
			t = parseInt(t, 10);

			if(nav_cnt === 20 || t > 0){
				clearInterval(navtop);
				
				if(t > 0){
					$(window).scroll(function(e){
						var wt = $(window).scrollTop();

						if(wt > t){
							$(nav).css("top", "0px");
						} else if(wt === 0){
							$(nav).css("top", t);  
						}
					});
				}
				
			}

			nav_cnt++;
		}, 100);
    }

	function callOnceElLoaded(d,f){
		//Time count
		var alt_cnt = 0;
	
		var elLoaded = setInterval(function() {
			//allowed Load Time
			var alt = 50;

			//element you want to test if exists on page
			var el = $(d);
			
			if (el.length > 0 || alt_cnt >= alt) {
				clearInterval(elLoaded);
				
				//fire callback function
				if(el) {
					f(el);
				}
				
			} else {
				alt_cnt++;
			}
		}, 100);
    }

    init();
})();
</script>
