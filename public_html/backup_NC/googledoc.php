<?php
//set_time_limit(0);
//require('functions.php');

//var_dump(openssl_get_cert_locations());
//echo "<br><br>";

require __DIR__ . '/vendor/autoload.php';

$client = new \Google_Client();
$client->setApplicationName('Google sheets and php');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__ . '/VirginAtlantic_2019-6427cb528508.json');

$service = new Google_Service_Sheets($client);
$spreadsheetID = '19BF-5i73qFG1Uh900zgJtpkLAb3H8fe6pPp1mNFJfJo';

//$range = "sheet1!A2:AC9";
$range = "sheet1!2:9";
$response = $service->spreadsheets_values->get($spreadsheetID, $range);
//echo "<pre>";
//var_dump($response);
//echo "</pre>";
$values = $response->getValues();

//echo "<pre>";

if(empty($values)){
	print "No data found";
} else {
	$mask = "%10s %-10s %s\n";
	
	//main routes array
	$routes_array = [];
	
	foreach($values as $row){
		//$ob = json_decode($row);
		//var_dump($row);

		$image = $row[1];
		$trip_type = $row[2];
		$origin_visual = $row[3];
		$origin = $row[4];
		$destination = $row[5];
		$hotel_ID = $row[6];
		$hotel_name = $row[7];
		$star_rating = $row[8]; 
		$destination_code_visual = $row[9];
		$data_destination = $row[10];
		$destination_code_formatted = $row[11];
		$cabin_visual = $row[12]; 
		$cabin_formatted = $row[13];
		$outbound_date_visual = $row[14];
		$flight_outbound_date_visual = $row[15];
		$inbound_date_visual = $row[16];
		$outbound_flight_formatted = $row[17];
		$outbound_date_formatted = $row[18];
		$inbound_flight_formatted = $row[19];
		$inbound_date_formatted = $row[20];
		$duration = $row[21];
		$container_link = $row[22];
		$ss_price = $row[23];
		$promo_box = $row[24];
		$promo_message = $row[25];
		$saving = $row[26];
		$hide_months = $row[27];
		$show_on_homepage = $row[28];
		
		
		//current date plus 7 days - day-month-year
		$departureDate = date("d-m-Y", time() + 604800);
		
		
		$obj = new stdClass;
		$obj->image = $image;
		$obj->trip_type = $trip_type;
		$obj->origin_visual = $origin_visual;
		$obj->origin_code = $origin;
		$obj->destination_visual = $destination;
		$obj->hotel_ID = $hotel_ID;
		$obj->hotel_name = $hotel_name;
		
		$obj->star_rating = $star_rating;
		$obj->destination_code_visual = $destination_code_visual;
		$obj->data_destination = $data_destination;
		$obj->destination_code_formatted = $destination_code_formatted;
		$obj->cabin_visual = $cabin_visual; 
		$obj->cabin_formatted = $cabin_formatted;
		$obj->outbound_date_visual = $outbound_date_visual;
		$obj->flight_outbound_date_visual = $flight_outbound_date_visual;
		$obj->inbound_date_visual = $inbound_date_visual;
		$obj->outbound_flight_formatted = $outbound_flight_formatted;
		$obj->outbound_date_formatted = $outbound_date_formatted;
		$obj->inbound_flight_formatted = $inbound_flight_formatted;
		$obj->inbound_date_formatted = $inbound_date_formatted;
		$obj->duration = $duration;
		$obj->container_link = $container_link;
		$obj->ss_price = $ss_price;
		$obj->promo_box = $promo_box;
		$obj->promo_message = $promo_message;
		$obj->saving = $saving;
		$obj->hide_months = $hide_months;
		$obj->show_on_homepage = $show_on_homepage;
		
		//get prices from either of the two APIs
		if($trip_type == 'Flight + Hotel'){
			
			$p = getFlightHotelPrice($origin, $destination, $departureDate, $hotel_ID);
			$obj->price = '£' . $p;

		} else if($trip_type == 'Flight'){
			
			//$p = '';
			//$p = getFlightPrice($origin, $destination_code_visual);
			$obj->price = getFlightPrice($origin, $destination_code_visual);
			//$obj->price = '£' . $p;
			
		}
		
		array_push($routes_array, $obj);
		
	}
}

//echo "</pre>";




















////putenv('GOOGLE_APPLICATION_CREDENTIALS=' . __DIR__ . '/client_secret.json');
//putenv('GOOGLE_APPLICATION_CREDENTIALS=' . __DIR__ . '/VirginAtlantic_2019-6427cb528508.json');
//$client = new Google_Client;
//$client->useApplicationDefaultCredentials();
//
//$client->setApplicationName("Something to do with my representatives");
//$client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);
//
//if ($client->isAccessTokenExpired()) {
//    $client->refreshTokenWithAssertion();
//}
//
//$accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
//
//use Google\Spreadsheet\DefaultServiceRequest;
//use Google\Spreadsheet\ServiceRequestFactory;
//
//ServiceRequestFactory::setInstance(
//    new DefaultServiceRequest($accessToken)
//);
//
//
//
//
//// Get our spreadsheet
//$spreadsheet = (new Google\Spreadsheet\SpreadsheetService)
//   ->getSpreadsheetFeed()
//   ->getByTitle('Garden notes');
//
//// Get the first worksheet (tab)
//$worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
//$worksheet = $worksheets[0];
//
//
//$listFeed = $worksheet->getListFeed();
//
///** @var ListEntry */
//foreach ($listFeed->getEntries() as $entry) {
//   $representative = $entry->getValues();
//}



?>