<style>.winter-sale-wrapper .WS-booking-price, .winter-sale-wrapper .WS-loc-txt, .winter-sale-wrapper .depFlight, .winter-sale-wrapper .destFlight, .winter-sale-wrapper .flightCode{font-weight:500;line-height:normal;letter-spacing:normal;font-stretch:normal;font-style:normal}.winter-sale-wrapper{float:none;width:100%;max-width:1180px;margin:auto;}.WS-flight-detail{float:left;width:100%}.winter-sale-wrapper{margin-bottom:60px;margin-top:60px;display:flex;flex-wrap:wrap}.WS-booking-price{float:right}.winter-sale-wrapper .depFlight, .winter-sale-wrapper .destFlight{font-family:gotham-medium;font-size:24px;color:#222}.winter-sale-wrapper .WS-loc-txt, .winter-sale-wrapper .flightCode{font-family:gotham-book;font-size:14px}.winter-sale-wrapper .flightCode{color:#a8a8a8;margin-left:10px}.winter-sale-wrapper .WS-loc-txt{color:#222;margin-right:15px;line-height:25px;display:inline-block}.winter-sale-wrapper .WS-loc-icn img{width:30px;margin-top:-3px;margin-right:7px}.winter-sale-wrapper .dubbleArrow{background:url(https://content.virginatlantic.com/content/dam/virgin-applications/fresh-air-core/1.0.18/images/Arrow_2.svg) 50% no-repeat!important;display:inline-block;height:27px;width:27px;top:3px;position:relative;margin-right:10px;margin-left:10px;zoom:0.7}.winter-sale-wrapper .WS-loc-img{float:left}.winter-sale-wrapper .WS-loc-img img{width:130px;height:134px;margin-right:20px}.winter-sale-wrapper .WS-flight-detail{border:1px solid #f3f3f3;padding:25px}.winter-sale-wrapper .WS-location-detail{float:left}.winter-sale-wrapper .WS-flight-location{float:left;padding-top:20px;width:60%}.ES-price-container{width:220px;display:block;float:right}.winter-sale-wrapper .WS-booking-price-link:before, .winter-sale-wrapper .WS-find-other-date-icn:before{font-style:normal;line-height:1;height:100%;margin-left:10px;display:inline-block;content:"\E90D"!important;font-variant:normal;text-transform:none;position:relative}.winter-sale-wrapper .WS-location-point{margin-bottom:10px}.winter-sale-wrapper .WS-booking-price-link{top:-7px;display:inline-block;position:relative}.winter-sale-wrapper .WS-find-other-date-txt{font-family:Gotham-book;font-size:12px;font-weight:500;color:#8b8b8b}.winter-sale-wrapper .WS-find-other-date-icn:before{color:#8b8b8b;font-family:icomoon!important;font-weight:400;font-size:6px;-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0)}.winter-sale-wrapper .WS-find-other-date-icn-active:before{-ms-transform:rotate(180deg);-webkit-transform:rotate(180deg);transform:rotate(180deg)}.winter-sale-wrapper .WS-find-other-date-icn{top:-1px;position:relative}.winter-sale-wrapper .WS-find-other-date-clossed{float:left;width:100%;text-align:center;padding:8px 0;cursor:pointer;border:1px solid #f3f3f3;border-top:0;border-bottom:1px solid #d8d7d7;background-color:rgba(243,243,243,0.25)}.winter-sale-wrapper .WS-booking-now, .winter-sale-wrapper .WS-find-other-date-open-content span{font-weight:500;letter-spacing:normal;text-align:right;font-family:Gotham-book;font-stretch:normal;font-style:normal}.winter-sale-wrapper .WS-booking-now{font-size:12px;line-height:normal;color:#e1163c;width:50px;display:inline-block;margin-right:5px}.winter-sale-wrapper .WS-booking-price sup{font-size:16px;color:#e1163c}.winter-sale-wrapper .WS-find-other-date-open-content span{font-size:14px;color:#222;margin-right:33px;float:left;line-height:30px}.winter-sale-wrapper .WS-find-other-date-open-content span a{color:#222;text-decoration:underline;float:none;margin-right:0}.winter-sale-wrapper .WS-find-other-date-open-content span sup{font-size:14px;top:0;color:#222}.winter-sale-wrapper .WS-find-other-date-open-content span.activeDate a, .winter-sale-wrapper .WS-find-other-date-open-content span.activeDate sup{color:#e1163c!important}.targetWinterSale{display:none}.winter-sale-wrapper .WS-find-other-date-open{float:left;width:100%;border:1px solid #f3f3f3;border-top:0;padding:20px;display:none}.winter-sale-wrapper .WS-filter-container{float:left;width:100%;margin-bottom:40px}.winter-sale-wrapper .WS-flight-anywhere, .winter-sale-wrapper .WS-flight-class, .winter-sale-wrapper .WS-flight-or-hotel, .winter-sale-wrapper .WS-flight-station{float:left;margin-right:1.7%;width:23.3%}.winter-sale-wrapper .WS-flight-class{margin-right:0!important}.winter-sale-wrapper .WS-flight-anywhere select, .winter-sale-wrapper .WS-flight-class select, .winter-sale-wrapper .WS-flight-or-hotel select, .winter-sale-wrapper .WS-flight-station select{width:100%;background:#fff;font-family:Gotham-book;font-size:16px;font-weight:400;font-style:normal;font-stretch:normal;line-height:1.5;letter-spacing: .1px;color:#222;height:50px;text-align:left;border-color:#f3f3f3;border-bottom:1px solid #d8d7d7}.winter-sale-wrapper .WS-flight-anywhere:after, .winter-sale-wrapper .WS-flight-class:after, .winter-sale-wrapper .WS-flight-or-hotel:after, .winter-sale-wrapper .WS-flight-station:after{color:#d31641;content:"\E90D"!important;font-family:icomoon!important;font-style:normal;font-weight:700;font-variant:normal;line-height:1;text-transform:none;font-size:6px;position:relative;height:100%;-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0);display:inline-block;top:-33px;right:-90%}.winter-sale-wrapper .loadMore{float:left;width:100%;text-align:center;font-family:gotham-light;color:#61126b;font-weight:700;font-size:16px;cursor:pointer}.loadMore:after{color:#61126b;font-family:icomoon!important;font-weight:400;font-size:7px!important;-ms-transform:rotate(0);-webkit-transform:rotate(0);transform:rotate(0)}.textandasset,.rtecomponent,.hovernavigation,.pagetabscomponent,#content>div.main.parsys.removeFocus>div.descriptivetext.section.removeFocus>div{display:none}.WS-flight-container{cursor:pointer;margin-bottom:30px;}.WS-flight-detail.WS-flight-hotel{margin-bottom:25px}.breadCrumbsComp .breadcrumb{width:75%}.winter-sale-wrapper .WS-flight-hotel .WS-location-detail{width:60% !important;float:left}.WS-flight-container{display:none}.addoffer{display:none}.WS-booking-price .WS-booking-offer{border:1px solid #61126b;border-bottom:2px solid;font-size:10px;padding:3px 8px;line-height:12px;color:#61126b;font-weight:500;display:block;width:90px;float:right}.winter-sale-wrapper .WS-booking-price{float:right;padding:8px 15px !important;font-family:gotham-medium;font-size:20px;text-align:center;width:100%;border:none;color:#fff;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;background:-webkit-linear-gradient(150deg, #a11453, #e1163c);background:-moz-linear-gradient(150deg, #e1163c, #a11453);background:-ms-linear-gradient(150deg, #e1163c, #a11453);background:linear-gradient(150deg, #e1163c, #a11453);-ms-filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e1163c', endColorstr='#A11453', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e1163c', endColorstr='#A11453', GradientType=0);cursor:pointer;margin-top:2em}.winter-sale-wrapper .WS-booking-price.ES-offer{border-radius:0px 0px 4px 4px;margin-top:0em}.WS-booking-price sup{color:#fff !important;top:0 !important}.WS-flight-detail span.flighttype{color:#e1163c;font-size:1.1em;display:block;font-weight:700;line-height:22px;font-family:'gotham-book';margin:0 0 15px 0}.WS-flight-detail span.fltOnly{color:#61126B}.winter-sale-wrapper .WS-flight-location{padding-top:0px;width:55% !important}.winter-sale-wrapper .WS-booking-now{font-size:16px;line-height:normal;color:#fff;width:auto;display:inline-block;margin-right:5px;font-family:'gotham-light'}.winter-sale-wrapper .depFlight, .winter-sale-wrapper .destFlight, .winter-sale-wrapper .destHotel{font-family:'gotham-book';font-size:18px;color:#222}.WS-dest-hotel span.WS-loc-icn{margin-right:7px}.WS-nights-icn{margin-left:10px;clear:left}.winter-sale-wrapper .WS-dest-hotel{margin-top:12px}.winter-sale-wrapper .destHotel{margin-right:10px}.winter-sale-wrapper .WS-booking-price-link{top:-4px !important}.svgflag .triangle{transform:rotate(-180deg);background-color:red;fill:green;stroke:green;stroke-width:4;transition:all 0.8s ease-in-out;transform-origin:15px 15px}.addOffer{float:right;color:#4D4D4D;font-family:'gotham-medium';width:100%;height:4em;background-color:#f9f9f9;border-radius:4px 4px 0px 0px;border:1px solid #ccc;text-align:center;vertical-align:middle;padding:0.7em 2.6em}.WS-was-price{display:block;font-size:0.8em}.secure-your-flight{height:36px;color:#757575;font-family:'Gotham-book';font-size:12px;font-weight:500;letter-spacing:0.01em;line-height:18px}.secure-container-text{width:100%;text-align:center;margin:1em 0;position:relative;float:right}.starClass-0{fill:#C11447}.starClass-1{fill:#D9D9D9}.WS-main-title{font-family:gotham-light;font-size:2.1rem;margin-bottom:26px;color:#e1163c;display:inline-block}.section.cardcd.card-d,.cutout-grid-contentcontainer,.introcomponent,footer,#tent_parsys_expander,.section-cta{float:left;width:100%}.winter-sale-wrapper select{-webkit-appearance:none}.winter-sale-wrapper select::-ms-expand{display:none}.section.cardcd.card-c .card-grid-component.content-component.card-component{float:left;width:100%}.WS-booking-price .addoffer{width:100%;position:relative;top:-30px;right:36px;float:right;height:0px;overflow:visible}.WS-viewSale{float:left;width:100%;text-align:center;font-family:gotham-light;color:#61126b;font-weight:700;font-size:16px;cursor:pointer}.WS-linkArrow{background:url(/content/dam/virgin-applications/images/staticpages/sprites/widget_panel_sprite.png) 0 -1113px no-repeat;width:20px;height:13px;margin:0 0 0 8px;display:inline-block}.winter-sale-wrapper .container a span{background:url(https://www.virginatlantic.com/content/dam/virgin-applications/images/staticpages/sprites/widget_panel_sprite.png) 0 -1113px no-repeat;width:20px;height:13px;margin:0 0 0 15px;display:inline-block}.winter-sale-wrapper .container a{float:right;color:#61126B;margin-right:30px;font-family:gotham-medium;margin-top:10px}.winter-sale-wrapper .container a:nth-child(2){color:#e1163c;margin-right:0px}span.WS-booking-offer{position:relative;margin:6px 6px;font-size:1.0em}@media only screen and (max-width : 992px){.WS-main-title{display:block}div.winter-sale-wrapper>div.container.WS-titles{text-align:center;margin-bottom:35px}div.winter-sale-wrapper > div.container.WS-titles a{float:none}.winter-sale-wrapper .container.WS-titles a:nth-child(2){margin-right:30px}.winter-sale-wrapper .WS-flight-anywhere, .winter-sale-wrapper .WS-flight-class, .winter-sale-wrapper .WS-flight-or-hotel, .winter-sale-wrapper .WS-flight-station{margin-right:2%;width:48%}.container.WS-flight-container{display:flex !important;width:46% !important;padding-right:0 !important;padding-left:0 !important;float:left !important;margin-left:2% !important;margin-right:2% !important;border:1px solid #f3f3f3;flex-direction:column;justify-content:space-between;margin-bottom:20px}.winter-sale-wrapper .WS-flight-detail{border:none}.winter-sale-wrapper .WS-find-other-date-clossed{margin-bottom:0px;border-top:1px solid #f3f3f3}.winter-sale-wrapper .depFlight, .winter-sale-wrapper .destFlight{display:inline-block}.winter-sale-wrapper .destFlight{margin-left:40px}.winter-sale-wrapper .WS-flight-anywhere:after, .winter-sale-wrapper .WS-flight-class:after, .winter-sale-wrapper .WS-flight-or-hotel:after, .winter-sale-wrapper .WS-flight-station:after{left:90% !important}.winter-sale-wrapper .WS-location-detail{width:100%}span.WS-outbound-date{display:inline-block}span.WS-nights, .winter-sale-wrapper .WS-loc-txt{display:block}span.WS-loc-icn{float:left}.winter-sale-wrapper .WS-loc-img{width:100%}.winter-sale-wrapper .WS-loc-img img{width:100%;height:auto}.winter-sale-wrapper .depFlight{margin-bottom:10px}.winter-sale-wrapper .WS-flight-location{width:100% !important}.WS-flight-detail span.flighttype{font-size:1.0em;margin:20px 0 15px 0}.winter-sale-wrapper .WS-booking-price{width:100%;text-align:center}.secure-container-text{width:100%}.winter-sale-wrapper .dubbleArrow{width:100%;display:none}.winter-sale-wrapper .WS-location-point{margin-bottom:15px}.winter-sale-wrapper .WS-dest-hotel{margin-top:20px}.winter-sale-wrapper .destHotel{display:block}.WS-dest-hotel span.WS-loc-icn{float:left;margin-right:0px}.winter-sale-wrapper span.destHotel{font-size:14px !important;font-weight:900;margin-bottom:12px}div.star{margin-left:40px;display:inline-block;}.secure-container-text{text-align:center}div.ES-price-container{width:100%;margin-top:20px}.WS-nights-icn{margin-left:0px;clear:left}}.red-tab-cont{display:block;text-align:right;}.red-tab{display:inline-block;padding: 7px 11px;border: solid 1px #d3d3d3;background-color:#dddddd;color:#61126b;border-bottom:none;font-family:'gotham-book';font-size:12px;height:30px;width:auto;}@media only screen and (max-width : 992px){.winter-sale-wrapper .WS-flight-anywhere, .winter-sale-wrapper .WS-flight-class, .winter-sale-wrapper .WS-flight-or-hotel, .winter-sale-wrapper .WS-flight-station{margin-right:2%;width:48%}.container.WS-flight-container{display:flex;width:46% !important;padding-right:0 !important;padding-left:0 !important;float:left !important;margin-left:2% !important;margin-right:2% !important;border:1px solid #f3f3f3;flex-direction:column;justify-content:space-between;margin-bottom:20px}.winter-sale-wrapper .WS-flight-detail{border:none}.winter-sale-wrapper .WS-find-other-date-clossed{margin-bottom:0px;border-top:1px solid #f3f3f3}.winter-sale-wrapper .depFlight, .winter-sale-wrapper .destFlight{display:inline-block}.winter-sale-wrapper .destFlight{margin-left:40px}.winter-sale-wrapper .WS-flight-anywhere:after, .winter-sale-wrapper .WS-flight-class:after, .winter-sale-wrapper .WS-flight-or-hotel:after, .winter-sale-wrapper .WS-flight-station:after{left:90% !important}.winter-sale-wrapper .WS-location-detail{width:100%}span.WS-outbound-date{display:inline-block}span.WS-nights, .winter-sale-wrapper .WS-loc-txt{display:block}span.WS-loc-icn{float:left}.winter-sale-wrapper .WS-loc-img{width:100%}.winter-sale-wrapper .WS-loc-img img{width:100%;height:auto}.winter-sale-wrapper .depFlight{margin-bottom:10px}.winter-sale-wrapper .WS-flight-location{width:100% !important}.WS-flight-detail span.flighttype{font-size:1.0em;margin:20px 0 15px 0}.winter-sale-wrapper .WS-booking-price{width:100%;text-align:center}.secure-container-text{width:100%}.winter-sale-wrapper .dubbleArrow{width:100%;display:none}.winter-sale-wrapper .WS-location-point{margin-bottom:15px}.winter-sale-wrapper .WS-dest-hotel{margin-top:20px}.winter-sale-wrapper .destHotel{display:block}.WS-dest-hotel span.WS-loc-icn{float:left;margin-right:0px}.winter-sale-wrapper span.destHotel{font-size:14px !important;font-weight:900;margin-bottom:12px}div.star{margin-left:40px;}.secure-container-text{text-align:center}div.ES-price-container{width:100%;margin-top:20px}.WS-nights-icn{margin-left:0px;clear:left}}@media only screen and (max-width : 576px){.winter-sale-wrapper .WS-flight-anywhere, .winter-sale-wrapper .WS-flight-class, .winter-sale-wrapper .WS-flight-or-hotel, .winter-sale-wrapper .WS-flight-station{margin-right:0px;width:100%}.container.WS-flight-container{width:100% !important;float:none !important;margin-right:auto !important;margin-left:auto !important}}@media only screen and (max-width : 415px){.container.WS-flight-container{margin-right:15px !important;margin-left:15px !important}}</style>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<div class="winter-sale-wrapper fresh-air targetWinterSale">
	<div class="container WS-titles">
		<h2 class="WS-main-title">What's hot in sale</h2>
		<a href="https://travelplus.virginatlantic.com/?cm_mmc=HP_CTA_FH_TGT&amp;utm_campaign=HP_CTA_FH_TGT&amp;utm_source=virginatlantic.com&amp;utm_medium=referral">Flight + Hotel<span></span></a>
		<a href="https://www.virginatlantic.com/gb/en/sale.html">Flight only<span></span></a>
	</div>
<?php

//require('flightonly_prices.php');
//require('flighthotel_prices.php');
set_time_limit(0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	
require('functions.php');
require('googledoc-no-api.php');

foreach($routes_array as $rowNumber => $route){
	
//var_dump($route);

?>
<?php if($route->show_on_homepage == "yes"){ ?>
<?php include('salemodule.php') ?>
<?php } ?>
<?php } ?>
<div class="WS-viewSale">
	<a href="https://www.virginatlantic.com/gb/en/sale.html">View more <img class="WS-linkArrow" src="https://stg-content.virginatlantic.com/content/dam/virgin-applications/images/staticpages/misc/clear.gif"></a>
</div>

	
</div>
<script>
	(function () {
	console.log('Entered campaign');
    var cnt = 10000;
    function init() {
		
		var htmlFltHot = '<span class="flighttype fltHotel">FLIGHT + HOTEL</span>';
		var htmlFltonly = '<span class="flighttype fltOnly">FLIGHT ONLY</span>';
		var myHotelDepositText = '<div class="secure-container-text"><span class="secure-your-flight">Secure your Flights+Hotel trip with deposits from just £175 pp</span></div>';
		
		if (typeof (jQuery) != 'undefined' && jQuery('.bg-dark.shopping-band-box').length>0 && jQuery('.winter-sale-wrapper').length<=1) {
			
			jQuery('.winter-sale-wrapper').insertAfter(jQuery('.bg-dark.shopping-band-box'));
			jQuery('.winter-sale-wrapper').removeClass('targetWinterSale');
			jQuery('.targetWinterSale').remove();
			
			jQuery('.WS-find-other-date-clossed').bind('click', function(){
				jQuery(this).parent().find('.WS-find-other-date-open').slideToggle('slow');
				jQuery(this).find('.WS-find-other-date-icn').toggleClass('WS-find-other-date-icn-active');
				
			});
			
			//sort by departure flight code
			sortByDepFlight();
			
			//sort by flight hotel
			sortByFlightStatus();
			
			//sort by flight class
			sortByFlightClass();
			
			//sort by destination flight
			sortByDestinationFlight();
			
			//redirect flight link
			flightClickLink();
			
			//load more
			loadMore();
			
			(function(){
                    setTimeout(function(){
                        var isAddPromo = true;
                        jQuery('.WS-flight-container').each( function(){
                            if(jQuery(this).data('flightstatus')=='flighthotel'){
                                jQuery(this).find(".WS-flight-location").prepend(htmlFltHot);
                                //jQuery(this).find('.WS-location-point').append(starHTML);
                                //jQuery(this).find('.WS-booking-price').after(myHotelDepositText);
                            }else{
                                jQuery(this).find(".WS-flight-location").prepend(htmlFltonly);
                            
                            }
//                            setTimeout(function(){
//                                jQuery(this).find('.flag').text(myStarSvg);
//                                
//                            },5000);    
                        });
                    },3000)
                
                })();
			
		} else if (cnt > 0) {
			cnt = cnt - 500;
			setTimeout(init, 500);
		} else {
		}
    }
	function sortByDepFlight(){
		jQuery('.WS-flight-station select').on('change', function(){
			dataSort();
		});
	}
	function sortByFlightStatus(){
		jQuery('.WS-flight-or-hotel select').on('change', function(){
			dataSort();
		});
	}
	
	function sortByFlightClass(){
		jQuery('.WS-flight-class select').on('change', function(){
			dataSort();
		});
	}
	
	function sortByDestinationFlight(){
		jQuery('.WS-flight-anywhere select').on('change', function(){
			dataSort();
		});
	}
	function dataSort(){
		console.log('1111');
		jQuery('.loadMore').hide();
		var flightclass = jQuery('.WS-flight-class select').children("option:selected").val();
		var flightStatus = jQuery('.WS-flight-or-hotel select').children("option:selected").val();
		var stationCode = jQuery('.WS-flight-station select').children("option:selected").val();
		var desFlightCode = jQuery('.WS-flight-anywhere select').children("option:selected").val();
	
			if(stationCode!='origin' && desFlightCode=='dest' && flightStatus=='triptype'){
				console.log('1');
				jQuery('.WS-flight-container').each( function(){
					if(jQuery(this).data('depflightcode')==stationCode && jQuery(this).data('flightclass')==flightclass){
						jQuery(this).fadeIn('10');
					}else{
						jQuery(this).fadeOut('10');
					}
				});
			}
			else if(stationCode=='origin' && desFlightCode!='dest' && flightStatus=='triptype'){
				console.log('11');
				jQuery('.WS-flight-container').each( function(){
					if(jQuery(this).data('desflightcode')==desFlightCode && jQuery(this).data('flightclass')==flightclass){
						jQuery(this).fadeIn('10');
					}else{
						jQuery(this).fadeOut('10');
					}
				});
			}
			else if(stationCode=='origin' && desFlightCode=='dest' && flightStatus!='triptype'){
				console.log('111');
				jQuery('.WS-flight-container').each( function(){
					console.log(flightStatus);
					if(jQuery(this).data('flightstatus')==flightStatus && jQuery(this).data('flightclass')==flightclass){
						console.log('true');
						jQuery(this).fadeIn('10');
					}else{
						console.log('false');
						jQuery(this).fadeOut('10');
					}
				});
			}
			else if(stationCode=='origin' && desFlightCode=='dest' && flightStatus=='triptype'){
				console.log('11');
				jQuery('.WS-flight-container').each( function(){
					if(jQuery(this).data('flightclass')==flightclass){
						jQuery(this).fadeIn('10');
					}else{
						jQuery(this).fadeOut('10');
					}
				});
			}
			else{
			
				if(stationCode=='origin'){
					console.log('11111');
					jQuery('.WS-flight-container').each( function(){
						if(jQuery(this).data('flightclass')==flightclass && jQuery(this).data('flightstatus')==flightStatus && jQuery(this).data('desflightcode')==desFlightCode){
							jQuery(this).fadeIn('10');
						}else{
							jQuery(this).fadeOut('10');
						}
					});
				}
				else if(desFlightCode=='dest'){
					console.log('111111');
					jQuery('.WS-flight-container').each( function(){
						if(jQuery(this).data('flightclass')==flightclass && jQuery(this).data('flightstatus')==flightStatus && jQuery(this).data('depflightcode')==stationCode){
							jQuery(this).fadeIn('10');
						}else{
							jQuery(this).fadeOut('10');
						}
					});
				}
				else if(flightStatus=='triptype'){
					console.log('1111111');
					jQuery('.WS-flight-container').each( function(){
						if(jQuery(this).data('flightclass')==flightclass && jQuery(this).data('depflightcode')==stationCode && jQuery(this).data('desflightcode')==desFlightCode){
							jQuery(this).fadeIn('10');
						}else{
							jQuery(this).fadeOut('10');
						}
					});
				}
				else{
					console.log('else');
					jQuery('.WS-flight-container').each( function(){
						if(jQuery(this).data('flightclass')==flightclass && jQuery(this).data('flightstatus')==flightStatus && jQuery(this).data('depflightcode')==stationCode && jQuery(this).data('desflightcode')==desFlightCode){
							jQuery(this).fadeIn('10');
						}else{
							jQuery(this).fadeOut('10');
						}
					});
				}
			}
	}
	function loadMore(){
		var size_li = jQuery(".winter-sale-wrapper .WS-flight-container").length;
		var x=20;
		jQuery('.winter-sale-wrapper .WS-flight-container:lt('+x+')').show();
		jQuery('.loadMore').click(function () {
			x= (x+20 <= size_li) ? x+20 : size_li;
			jQuery('.winter-sale-wrapper .WS-flight-container:lt('+x+')').show();
			if (x === size_li) {
				jQuery('.loadMore').hide();
			}
		});
	}
	function flightClickLink(){
		jQuery('.WS-flight-detail').bind('click', function(){
			window.location.href = jQuery(this).parent().data('flightlink');
		});
	}
    init();
  })();
</script>
