<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://travelplus.virginatlantic.com/travelplus/cjs-search-api/search",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"location\":\"orlando\",\"gateway\":\"LGW\",\"bookingType\":\"holiday\",\"departureDate\":\"11-09-2019\",\"duration\":\"8\",\"partyCompositions\":[{\"adults\":2,\"childAges\":[],\"infants\":0}]}",
  CURLOPT_HTTPHEADER => array(
    //"Postman-Token: 707cd561-ae1d-4fa1-b4d9-01eca72c3c85",
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
	
?>
	
</body>
</html>