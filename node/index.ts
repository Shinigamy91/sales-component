import app from "./App";

const port = 5050;

app.listen(port, () => {
    return console.log(`Backend Server is listening on ${port}`);
});
