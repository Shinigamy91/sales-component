import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import session from "express-session";

import appConfig from "./config/appconfig";
import AuthService from "./services/AuthService";

class App {
    public express: express.Application;
    public router: express.Router;
    private authService: any;

    public constructor() {
        this.express = express();
        this.router = express.Router();

        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
        this.express.use(cookieParser());
        this.express.use(cors());
        this.express.use(session({ secret: appConfig.sessionSecret }));
        this.express.use(express.static(`${__dirname}/public`));
        this.express.options("*", cors());
        this.express.set("view engine", "jsx");
        this.express.set("views", `${__dirname}/views`);
        this.express.engine("jsx", require("express-react-views").createEngine());

        this.express.use(express.static(`${__dirname}/views`));
        this.express.use("/", this.router);

        this.authService = new AuthService();
        this.mountRoutes();
    }

    private mountRoutes(): void {
        this.pages(this.router);
    }

    private pages(router: express.Router): void {
        const auth = require("./middleware/auth");

        router.get("/", auth, (req, res) => {
            res.render("index");
        });

        router.get("/login", (req, res) => {
            res.render("login");
        });

        router.post("/login", (req, res) => {
            this.authService.authenticate(req.body).subscribe((token: string) => {
                if (token) {
                    res.cookie("x-access-token", token);
                    res.redirect('/');
                } else {
                    res.redirect('/login');
                }
            });
        });
    }
}

export default new App().express;
