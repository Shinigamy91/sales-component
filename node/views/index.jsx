const React = require('react');
const DefaultLayout = require('./layouts/default.jsx');

class HomePage extends React.Component {
  render() {
    return (
      <DefaultLayout>
        <h2>Dynamic Sales component</h2>
        <form>
          <button className="btn btn-primary" id="generateBtn">Generate HTML</button>
          <div className="form-group">
            <label for="exampleFormControlTextarea1">HTML code</label>
            <textarea className="form-control" id="htmlTextarea" rows="20" readOnly></textarea>
          </div>
        </form>
        <script src="js/script.js"></script>
      </DefaultLayout>
    );
  }
}

module.exports = HomePage;
