const React = require('react');
const DefaultLayout = require('./layouts/default.jsx');

class LoginPage extends React.Component {
  render() {
    return (
      <DefaultLayout>
        <h1>Login Page</h1>
        <form action="/login" method="post">
            <div className="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="text" className="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Enter username" />
            </div>
            <div className="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" className="form-control" id="password" name="password" placeholder="Enter Password" />
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </DefaultLayout>
    );
  }
}

module.exports = LoginPage;
