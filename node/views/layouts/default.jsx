const React = require('react');

class DefaultLayout extends React.Component {
  render() {
    return (
        <html dir="ltr" lang="en-US">
            <head>
                <title>Sales Component Generator App</title>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossOrigin="anonymous" />
            </head>

            <body>
                <div className="container">
                    {this.props.children}
                </div>
            </body>
        </html>
    );
  }
}

module.exports = DefaultLayout;
