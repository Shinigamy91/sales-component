import jwt from "jsonwebtoken";
import config from "../config/appconfig";

module.exports = (req: any, res: any, next: any) => {
  // get the token from the header if present
  const token = req.cookies["x-access-token"];
  // if no token found, return response (without going to the next middelware)
  if (!token) {
      return res.redirect("/login");
    }

  try {
    // if can verify the token, set req.user and pass to next middleware
    const decoded = jwt.verify(token, config.jwtSecret);
    req.username = decoded;
    next();
  } catch (ex) {
    // if invalid token
    res.redirect("/login");
  }
};
