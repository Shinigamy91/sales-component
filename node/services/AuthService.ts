import jwt from "jsonwebtoken";
import { Observable } from "rxjs";
import appConfig from "../config/appconfig";

class AuthService {
    public authenticate(requestBody: any): Observable<Boolean | Error> {
        return this.comparePasswords(requestBody.password, appConfig.user.password, requestBody.username);
    }

    private comparePasswords(requestPass: string, userPass: string, userName: string): Observable<Boolean> {
        return Observable.create((observer: any) => {
            if (userName === appConfig.user.username && requestPass === userPass) {
                const token = jwt.sign({
                    auth: true,
                    id: userName,
                    tokenDate: new Date(),
                }, appConfig.jwtSecret);

                observer.next(token);
                observer.complete();

            } else {
                observer.next(false);
                observer.complete();
            }
        });
    }
}

export default AuthService;
