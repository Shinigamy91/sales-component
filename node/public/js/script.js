(function() {
    const button = document.querySelector('#generateBtn');
    const output = document.querySelector('#htmlTextarea');

    button.addEventListener('click', event => {
        event.preventDefault();
        fetch('http://35.246.38.161:5555/blackfriday-text.php').then(resp => resp.text())
            .then(text => {
                output.textContent = text
            })
    });
})();